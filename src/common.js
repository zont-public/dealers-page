import $ from 'jquery';


export const rusDateOnly = (date) => `${pad0(date.getDate(), 2)}.${pad0(date.getMonth() + 1, 2)}.${date.getFullYear()}`;
export const rusTimeOnly = (date, withSeconds = false) => {
    const seconds = withSeconds ? `:${pad0(date.getSeconds(), 2)}` : '';
    return `${pad0(date.getHours(), 2)}:${pad0(date.getMinutes(), 2)}${seconds}`;
};

export const rusDateTime = (date, withSeconds = false) => `${rusDateOnly(date)} ${rusTimeOnly(date, withSeconds)}`;
export const rusTimeDate = (date, withSeconds = false) => `${rusTimeOnly(date, withSeconds)} ${rusDateOnly(date)}`;

export const rusTimeDateBr = (date, withSeconds = false) =>
    `${rusTimeOnly(date, withSeconds)}<br/>${rusDateOnly(date)}`;

export const rusTimeDateBrSmall = (date, withSeconds = false) =>
    `${rusTimeOnly(date, withSeconds)}<br/><span style="font-size:80%">${rusDateOnly(date)}</span>`;

export const rusDateNoYear = (date) => `${pad0(date.getDate(), 2)}.${pad0(date.getMonth() + 1, 2)}`;
export const rusTimeDateNoYear = (date) => `${rusTimeOnly(date)} ${rusDateNoYear(date)}`;



// https://github.com/dperish/prettyFloat.js
export const prettyFloat = function (value, precision, localize) {
    value = value !== undefined ? value : "";

    const rounded = (!isNaN(precision) && parseInt(precision, 10) > 0)
        ? parseFloat(value).toFixed(parseInt(precision, 10))
        : value;

    const trimmed = parseFloat(rounded).toString();

    if (localize && !isNaN(trimmed)) {
        return parseFloat(trimmed).toLocaleString();
    }

    return trimmed;
}



export const pad0 = function (str, width) {
    str = str.toString()
    while (str.length < width)
        str = '0' + str
    return str
}

export const isFloat = function (str) {
    // http://stackoverflow.com/questions/18082/validate-numbers-in-javascript-isnumeric
    str = str.trim().replace(',', '.')
    return !isNaN(parseFloat(str)) && isFinite(str)
}

export const parseFloatWithComma = function (str) {
    if (str == null) return null
    if (typeof str == 'string' && str.trim() == '') return null
    if (typeof str == 'number') return str
    if (!isFloat(str)) return NaN

    return parseFloat(str.replace(',', '.'))
}

export const isInteger = function (str)
{
    return !isNaN(parseInt(str)) && (parseFloat(str) == parseInt(str))
}


export const parseRusDate = function (str) {
    var re = /^(\d\d?).(\d\d?).((?:1\d|20\d\d))$/
    var m = re.exec(str)
    if (m == null) return null

    var day = parseInt(m[1], 10)
    var month = parseInt(m[2], 10)
    var year = parseInt(m[3], 10)

    if (day < 0 || day > 31) return null
    if (month < 1 || month > 12) return null

    return new Date(
        year < 100 ? (2000 + year) : year,
        month - 1,
        day
    )
}


export const isSameDay = function (date1, date2) {
    return date1.getFullYear() == date2.getFullYear() &&
        date1.getMonth() == date2.getMonth() &&
        date1.getDate() == date2.getDate()
}


export const hasBit = function (value, bit) {
    return (value & (1 << bit)) != 0
}
export const hasBitVal = function (value, bitVal) {
    return (value & bitVal) != 0
}


export const plural = function (num, form1, form234, formMany) {
    var rem10 = num % 10
    var rem100 = num % 100

    if (rem100 > 10 && rem100 < 20)
        return formMany

    if (rem10 == 1) return form1
    if (rem10 >= 2 && rem10 <= 4) return form234

    return formMany
}


export const django_csrfcookie = function () {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = $.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    return getCookie('csrftoken')
}


export const binarySearch = function (data, value, findPositionForNew) {
    var r = data.length, l = -1, m;
    while (r - l > 1)
        if (data[m = r + l >> 1] < value) l = m;
        else r = m;
    return data[r] != value ? (findPositionForNew ? r : -1) : r;
}

export const binarySearchByField = function (data, field, value) {
    var r = data.length, l = -1, m;
    while (r - l > 1)
        if (data[m = r + l >> 1][field] < value) l = m
        else r = m

    return {l: l, r: r}
}

export const binarySearchByIndex0 = function (data, value) {
    return binarySearchByField(data, 0, value)
}


export const arraysEqual = function (a, b) {
    if (a.length != b.length) return false

    for (var i = a.length - 1; i >= 0; i--)
        if (a[i] != b[i])
            return false

    return true
}


export const escape_html = function (text) {
    return text.toString()
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;')
}

export const nl2br = function (text) {
    return text.replace(/\n/g, '<br/>')
}
