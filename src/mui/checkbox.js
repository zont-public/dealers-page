import $ from 'jquery';

import './_checkbox.sass';


$(function () {

    $(document).on('click', '.mui_checkbox', function () {
        if (! $(this).hasClass('mui_checkbox_disabled'))
            $(this)
                .toggleClass('mui_checkbox_checked')
                .change()
    })

});
