var Class = function (parent, prot) {
    if (arguments.length == 1)
    {
        prot = parent;
        parent = undefined;
    }


    var Constructor = function() {
        if (this.init)
        {
            this.init.apply(this, arguments);
            return this;
        }
        else
            return this;
    }

    var ctor = Constructor;

    if (parent)
    {
        ctor.prototype = Object.create(parent.prototype);
        ctor.superclass = parent.prototype;
    }
    else
        ctor.prototype = {};


    for (var i in Class.class_methods)
        ctor[i] = Class.class_methods[i];

    ctor.implement(Class.common_methods);
    ctor.implement(prot);


    ctor.prototype.constructor = ctor;

    return ctor;
};

Class.class_methods = {
    implement: function (iface) {
        for (var name in iface) {
            this.prototype[name] = iface[name];
            if (typeof this.prototype[name] == 'function')
            {
                this.prototype[name].$class = this;
                this.prototype[name].$name = name;
            }
        }

        return this;
    }
};


Class.common_methods = {
    base: function() {
        throw new Error('Class.base() is deprecated')

        // var caller = arguments.callee.caller;

        // if (! caller.$class)
        //     throw new Error('Class.base() method can only be called inside method of the class');
        // if (! caller.$class.superclass)
        //     throw new Error('No superclass');

        // return caller.$class.superclass[arguments.callee.caller.$name].apply(this, arguments);
    }
};


Class.Factory = function (klass, args) {
    function F() {
        return klass.apply(this, args);
    }
    F.prototype = klass.prototype;

    return function () {
        return new F();
    };
};



var Binding = Class({
    init: function (obj, handlers) {
        this.obj = obj;
        this.handlers = handlers;

        if (! obj.$listeners) obj.$listeners = {};

        for (var i in this.handlers)
        {
            if (obj.$listeners.hasOwnProperty(i))
                obj.$listeners[i].push(this.handlers[i]);
            else
                obj.$listeners[i] = [this.handlers[i]];
        }
    },

    unbind: function () {
        for (var i in this.handlers)
            this.obj.unbind(i, this.handlers[i]);

        delete this.obj;
        delete this.handlers;
    }
});

Class.EventSource = {
    bind: function () {
        if (arguments.length == 2)
        {
            var obj = {};
            obj[arguments[0]] = arguments[1];
            return this.bind(obj);
        }

        return new Binding(this, arguments[0]);
    },

    unbind: function (eventname, callback) {
        if (this.$listeners[eventname])
            for (var i = 0, len = this.$listeners[eventname].length; i < len; i++)
                if (this.$listeners[eventname][i] == callback)
                    this.$listeners[eventname].splice(i, 1);
    },

    unbindAll: function () {
        delete this.$listeners;
    },

    bindWildcard: function (callback) {
        if (! this.$wildcardListeners) this.$wildcardListeners = []

        this.$wildcardListeners.push(callback)
    },

    unbindWildcard: function (callback) {
        // for (var i in this.$wildcardListeners)
        for (var i = 0, len = this.$wildcardListeners.length; i < len; i++)
            if (this.$wildcardListeners[i] == callback)
                this.$wildcardListeners.splice(i, 1)
    },

    notify: function (eventname, params) {
        params = params || [] // For IE8

        if (! (params instanceof Array))
            params = Array.prototype.slice.apply(params)

        if (this.$listeners && this.$listeners[eventname])
        {
            var listeners = this.$listeners[eventname];
            for (var i = 0; i < listeners.length; i++)
                listeners[i].apply(this, params);
        }

        if (this.$wildcardListeners)
        {
            var wildcardParams = [ this, eventname ].concat(params)

            // for (var i in this.$wildcardListeners)
            for (var i = 0, len = this.$wildcardListeners.length; i < len; i++)
                this.$wildcardListeners[i].apply(this, wildcardParams)
        }

        // May be we will return something more interesting in future
        // for example, result of first/last listener
        return null;
    },

    hasListeners: function (eventname) {
        return this.$listeners && this.$listeners[eventname] && this.$listeners[eventname].length > 0;
    },

    notifier: function (eventname, bound_args) {
        return (function () {
            var args;
            if (bound_args)
                args = Array.prototype.slice.apply(bound_args);
            else
                args = [];
            Array.prototype.push.apply(args, arguments);
            return this.notify(eventname, args);
        }).bind(this);
    },

    chain: function (eventname, other, otherevent) {
        if (typeof eventname == 'string')
            this.bind(eventname, other.notifier(otherevent ? otherevent : eventname));
        else
            for (var i in eventname)
                this.bind(eventname[i], other.notifier(otherevent ? otherevent : eventname[i]));
        return this;
    }
};



Class.Properties = function(props) {
    var mixin = {
        get: function (propname) {
            return this[propname]();
        },

        set: function () {
            if (arguments.length == 1)
            {
                for (var i in arguments[0])
                    if (this[i])
                        this[i](arguments[0][i]);
            }
            else
                if (this[arguments[0]])
                    this[arguments[0]].apply(this, Array.prototype.slice.call(arguments, 1));

            return this
        }
    }

    for (var name in props) {
        mixin[name] = (function (name, params) {
            var prop_storage = '_' + name;

            if (params.hasOwnProperty('value'))
                mixin[prop_storage] = params.value;

            return function () {
                if (arguments.length == 0)
                {
                    if (params.get)
                        return params.get.apply(this);
                    else
                        return this[prop_storage];
                }
                else
                {
                    if (params.set)
                        params.set.apply(this, arguments);
                    else
                        this[prop_storage] = arguments[0];


                    // Array.prototype.slice.call is for turning arguments to real array
                    if (params.event === true)
                        this.notify(name, arguments)
                    else if (params.event)
                        this.notify(params.event, arguments)

                    return this;
                }
            };
        })(name, props[name]);
    }

    return mixin;
}



Class.Singleton = function (klass) {
    var instance = null
    var s = Class(klass, {
        init: function () {
            instance = this
            if (klass.prototype.init)
                klass.prototype.init.apply(this, arguments)
        }
    })
    s.instance = function () {
        if (instance) return instance
        new s
        return instance
    }
    return s
}


export default Class;
