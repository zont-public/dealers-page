import './_basedialog.sass';

import $ from 'jquery';
import 'jquery-ui/ui/widget';
import 'jquery-ui/ui/position';

import {push_back_item, pop_back_item} from 'console/back-button'


$.widget('mui.basedialog', {
    options: {
        outsideclick: function() { return true },

        z_index: null,
        dimmer_z_index: null,

        scaleToFit: true
    },

    dimmer: null,
    wrapper: null,

    _create: function() {
        var self = this, options = this.options

        this.dimmer =
            $('<div class="mui_basedialog_dimmer">')
                .click(function() {
                    if (self._trigger('outsideclick', null, []) == true)
                        self.close()
                    return false
                })
            .on('transitionend webkitTransitionEnd', (function () {
                if (! this.dimmer.hasClass('mui_basedialog_visible'))
                    this.dimmer.css('display', 'none')
            }).bind(this))

        this.wrapper = $('<div>', { 'class': 'mui_basedialog' })
            .append(
                this.content = $('<div>', { 'class': 'mui_basedialog_content' })
                    .append(this.element)
            )
            .append('<div class="mui_basedialog_close">')
            .on('transitionend webkitTransitionEnd', (function () {
                if (! this.wrapper.hasClass('mui_basedialog_visible'))
                    this.wrapper.css('display', 'none')
            }).bind(this))

        if (this.element.attr('dialog-class'))
            this.wrapper.addClass(this.element.attr('dialog-class'))

        this.wrapper.find('.mui_basedialog_close')
            .click(function() { self.close() })


        if (this.options.z_index != null)
            this.wrapper.css('z-index', this.options.z_index)
        if (this.options.dimmer_z_index != null)
            this.dimmer.css('z-index', this.options.dimmer_z_index)


        this.dimmer.appendTo('body')
        this.wrapper.appendTo('body')


        this._reposition_bound = this.reposition.bind(this)
    },

    destroy: function() {
        this.wrapper.find('.mui_basedialog_close').remove()
        this.element.siblings().remove()
        this.element.unwrap().unwrap()
        this.dimmer.add(this.wrapper).remove()
        this.element.hide()

        delete this.dimmer
        delete this.wrapper

        $.Widget.prototype.destroy.call(this)
    },


    _clearScale: function () {
        this.wrapper
            .css({
                '-o-transform': '',
                '-ms-transform': '',
                '-moz-transform': '',
                '-webkit-transform': '',
                'transform': ''
            })
    },


    reposition: function () {
        this._clearScale()

        this.wrapper
            .position({
                my: 'center',
                at: 'center',
                of: $(window), //this.dimmer,
                offset: '0 ' + this.wrapper.css('margin-top').replace('px', '')
            })


        if (this.options.scaleToFit)
        {
            // Downscaling to screen size
            var gap = 10
            var h_scale = $(window).innerWidth() / (this.wrapper.outerWidth() + 2 * gap)
            var v_scale = $(window).innerHeight() / (this.wrapper.outerHeight() + 2 * gap)
            var scale = Math.min(h_scale, v_scale)
            if (scale < 1.0)
            {
                var css_scale = 'scale(' + scale.toFixed(3) + ')'
                this.wrapper.css({
                    '-o-transform':      css_scale,
                    '-ms-transform':     css_scale,
                    '-moz-transform':    css_scale,
                    '-webkit-transform': css_scale,
                    'transform':         css_scale
                })
            }
        }
    },


    _init: function() {
        if (this.wrapper.hasClass('mui_basedialog_visible'))
            pop_back_item();

        this.wrapper.add(this.dimmer)
            .show()
            .addClass('mui_basedialog_visible')

        this.element.show()

        this.reposition()

        $(window).on('orientationchange', this._reposition_bound)

        // Chrome's behavior is strange when history.back() is followed immediately by history.push_state():
        // 1. history.pushState({x: 1}, null, null);
        // 2. history.back(); history.pushState({x: 2}, null, null);
        // 3. console.log(history.state); // ← null instead of {x: 2}
        // Seems like pushState is ignored when called immediately after back(), setImmediate
        // doesn't help either, so using setTimeout(, 1).
        setTimeout(() => {
            push_back_item(() => this.close());
        }, 1);
    },


    close: function() {
        if (this.options.canClose && this.options.canClose.apply(this.element) == false)
            return

        pop_back_item();

        this.wrapper.removeClass('mui_basedialog_visible').addClass('mui_basedialog_postshow')
        this.dimmer.removeClass('mui_basedialog_visible')

        setTimeout((function() {
            if (this.wrapper)
            {
                this.wrapper
                    .removeClass('mui_basedialog_postshow')
                    .css('display', 'none')
                this._clearScale()
            }

            this._trigger('close')
        }).bind(this), 250)

        $(window).off('orientationchange', this._reposition_bound)
    }
});
