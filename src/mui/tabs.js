import './_tabs.sass';

import $ from 'jquery';
import 'jquery-ui/ui/widget';


$.widget('mui.mui_tabs', {
    options: {
    },

    content: null,

    _create: function() {
        var self = this

        var bar = $('<div class="mui_tabs_bar">')

        this.element
            .addClass('mui_tabs')
            .wrapInner('<div class="mui_tabs_content">')
            .prepend(bar)

        this.content = this.element.children('.mui_tabs_content')

        this.content.children().each(function() {
            var tab = $(this)
            bar.append(
                $('<div class="mui_tabs_handle">')
                    .text(tab.attr('title'))
                    .click(function() {
                        $(this)
                            .addClass('mui_tabs_active')
                            .siblings()
                                .removeClass('mui_tabs_active')

                        self.content.children().hide()
                        tab.show()
                    })
            )
        })

        this.element.children('.mui_tabs_bar').children().eq(0).click()
    }
});
