import './_buttonbox.sass';

import $ from 'jquery';
import 'jquery-ui/ui/widget';


$.widget('mui.buttonbox', {
    options: {
        buttons: [],
        rtl: false
    },

    box: null,

    _create: function() {
        this.element.addClass('mui_buttonbox')
    },

    _init: function () {
        this.element.find('button').remove()

        var self = this
        $.each(this.options.buttons, function() {
            var button = $('<button>')
                .text(this.text)
                .click(this.click)
                .toggleClass('warning', !!this.warning)

            if (this.id)
                button.attr('button_id', this.id)

            if (self.options.rtl)
                self.element.prepend(button)
            else
                self.element.append(button)
        })
    },

    button: function (id) {
        return this.element.find('[button_id="' + id + '"]')
    },

    disabled: function (val) {
        if (arguments.length == 0)
        {
            var all_disabled = true
            this.element.find('button').each(function () {
                if (! $(this).prop('disabled'))
                {
                    all_disabled = false
                    return false
                }
            })

            return all_disabled
        }

        this.element.find('button').prop('disabled', !!val)
    }
});
