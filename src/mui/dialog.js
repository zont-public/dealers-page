import './_dialog.sass';

import $ from 'jquery';
import 'jquery-ui/ui/widget';

import 'mui/basedialog';
import 'mui/buttonbox';


$.widget('mui.mui_dialog', $.mui.basedialog, {
    options: {
        error: false,

        buttons: [],

        wide_padding: false
    },

    _create: function() {
        $.mui.basedialog.prototype._create.call(this)

        this.element.closest('.mui_basedialog')
            .toggleClass('mui_dialog_error', this.options.error == true)

        this.element.addClass('mui_dialog_content')

        if (this.options.wide_padding)
            this.element.addClass('mui_dialog_wide_padding')
    },

    _init: function () {
        $.mui.basedialog.prototype._init.call(this)

        if (this._buttonbox)
        {
            this._buttonbox.remove()
            delete this._buttonbox
        }

        if (this.options.buttons.length > 0)
        {
            this._buttonbox = $('<div>', { 'class': 'mui_dialog_buttonbox' })
                .buttonbox({
                    buttons: $.map(this.options.buttons, (function(btn) {
                        return {
                            warning: btn.warning,
                            text: btn.text,
                            click: btn.click.bind(this.element)
                        }
                    }).bind(this))
                })

            this.content.append(this._buttonbox)
        }

        this.reposition()
    },

    destroy: function() {
        this.element.removeClass('mui_dialog_content')
        if (this._buttonbox)
        {
            this._buttonbox.remove()
            delete this._buttonbox
        }

        $.mui.basedialog.prototype.destroy.call(this)
    },


    buttonsdisabled: function (disable) {
        if (arguments.length == 0)
            return this.content.find('.mui_dialog_buttonbox').buttonbox('disabled')
        else
            this.content.find('.mui_dialog_buttonbox').buttonbox('disabled', disable)
    }
});


$.mui.mui_dialog.messagebox = function (options) {
    return ($('<div>')
        .css('width', options.width ? options.width : 'auto')
        .append($('<h2 class="mui_dialog_title"></h2>').text(options.title))
        .append(options.message)
        .mui_dialog($.extend({}, options, {
            close: function() {
                $(this).mui_dialog('destroy')
                $(this).remove()

                if (options.close)
                    options.close.apply(this)
            }
        })))
};

$.mui.mui_dialog.errorbox = function (options) {
    return $.mui.mui_dialog.messagebox($.extend(options, {error:true}))
};
