import './_miniprogressbar.sass';

import $ from 'jquery';
import 'jquery-ui/ui/widget';


$.widget('mui.miniprogressbar', {
    options: {
        percent: 60
    },

    _create: function () {
        this.element
            .addClass('mui_miniprogressbar')
            .html(
                '<div class="mui_miniprogressbar_filler"></div>'
              + '<div class="mui_miniprogressbar_percent"></div>'
            )

        this._applyPercent()
    },

    destroy: function () {
        this.element
            .removeClass('mui_miniprogressbar')
            .html('')
    },

    _applyPercent: function () {
        this.element.children('.mui_miniprogressbar_filler')
            .width(this.options.percent + '%')
        this.element.children('.mui_miniprogressbar_percent')
            .text(this.options.percent + '%')
    },

    percent: function () {
        if (arguments.length == 0)
            return this.options.percent

        this.options.percent = arguments[0]
        this._applyPercent()
    }
});
