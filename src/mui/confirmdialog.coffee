import Promise from 'promise'

import {nonreact_messagebox} from 'console/dialogs'


export default ({title, message='', warning=no, no_text='отмена', yes_text='продолжить'}) ->
    new Promise (resolve, reject) ->
        {close} = nonreact_messagebox
            error: !!warning
            size: 'medium'
            title: title
            message: message
            on_close: -> reject()
            buttons: [
                {
                    text: no_text
                    click: ->
                        reject()
                        close()
                }
                {
                    text: yes_text
                    warning: !!warning
                    click: ->
                        resolve()
                        close()
                }
            ]
