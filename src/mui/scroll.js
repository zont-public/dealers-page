import './_scroll.sass';

import $ from 'jquery';
import 'jquery-ui/ui/widget';
import 'jquery-ui/ui/widgets/draggable';


var scrollable = {
    thumbMinHeight: 32,

    defaultCalcThumb: function (contentHeight, windowHeight, scrollPos) {
        if (contentHeight <= windowHeight)
            return {
                visible: false
            }
        else
        {
            var thumbSize = Math.max(windowHeight * windowHeight / contentHeight, scrollable.thumbMinHeight)

            return {
                visible: true,
                size: thumbSize,
                pos : scrollPos * (windowHeight - thumbSize) / (contentHeight - windowHeight)
            }
        }
    },

    defaultCalcScroll: function (contentHeight, windowHeight, thumbPos, thumbSize) {
        return {
            scroll: Math.max(thumbPos, 0) / (windowHeight - thumbSize) * (contentHeight - windowHeight),
            setPos: null
        }
    }
}

export default scrollable;

$.widget('mui.scrollable', {
    options: {
        scrollStep: 50,

        touchDragThreshold: 8,

        calcThumb: scrollable.defaultCalcThumb,
        calcScroll: scrollable.defaultCalcScroll
    },

    _create: function() {
        this.element
            .addClass('mui_scrollable')
            .wrapInner('<div class="mui_scrollable_content">')

        this._content = this.element.children()

        if (true) //Modernizr.cssscrollbar)
        {
            this._native = true
            this.element.addClass('mui_scrollable_native')
            return
        }


        var self = this
        var scrollable = this.element
        this._thumb = $('<div>', { 'class': 'mui_scrollable_thumb' })
            .append($('<div>', { 'class': 'mui_scrollable_arrow mui_scrollable_arrow_up' }))
            .append($('<div>', { 'class': 'mui_scrollable_arrow mui_scrollable_arrow_down' }))


        this.events_map = {
            'touchstart': this._onTouchStart.bind(this),
            'touchmove' : this._onTouchMove.bind(this),
            'touchend'  : this._onTouchEnd.bind(this),
            'mousewheel': this._onMouseWheel.bind(this)
        }

        scrollable.on(this.events_map)

        this._thumb.draggable({
            containment: 'parent',
            axis: 'y',
            start: function() { $(this).parent().addClass('mui_scrollable_active') },
            stop: function() { $(this).parent().removeClass('mui_scrollable_active') },
            drag: function(event, ui) {
                var scrollbar = $(this).parent()

                var scroll_state = self.options.calcScroll(
                    self._content.outerHeight(),
                    scrollbar.height(),
                    ui.position.top,
                    $(this).height()
                )
                self._setScroll(scroll_state.scroll, true)

                if (scroll_state.setPos)
                {
                    // hack: this isn't documented, but you can set desired
                    // draggable position by setting ui.position
                    ui.position = scroll_state.setPos
                }
            }
        })

        $('<div class="mui_scrollable_bar">')
            .append(this._thumb)
            .appendTo(this.element)

        this._updateThumb()
    },

    destroy: function () {
        this.element.removeClass('mui_scrollable')

        if (! this._native)
        {
            this.element
                .off(this.events_map)
                .children('.mui_scrollable_bar').remove().end()
        }

        this._content.children().unwrap()
    },

    _onTouchStart: function (e) {
        this.touch_start_y = e.originalEvent.touches[0].pageY
        this.touch_start_scroll = this.offset()
        this.touch_was_drag = false
        e.preventDefault()
    },

    _onTouchMove: function (e) {
        var touch = e.originalEvent.changedTouches[0]
        if (Math.abs(touch.pageY - this.touch_start_y) > this.options.touchDragThreshold)
        {
            this.touch_was_drag = true
            this._setScroll(this.touch_start_scroll + this.touch_start_y - e.originalEvent.touches[0].pageY)
        }
    },

    _onTouchEnd: function (e) {
        if (!this.touch_was_drag)
        {
            $(e.target).trigger('mousedown')
            $(e.target).trigger('click')
            $(e.target).trigger('mouseup')
        }
    },

    _onMouseWheel: function (e, ui) {
        this._setScroll(this.offset() - this.options.scrollStep * ui)
        return false
    },

    _setScroll: function(y, by_thumb) {
        if (this._native)
        {
            this.element.scrollTop(y)
        }
        else
        {
            if (! by_thumb)
            {
                if (y > this._content.outerHeight() - this.element.height())
                    y = this._content.outerHeight() - this.element.height()
                if (y < 0) y = 0
            }

            this._content.css('top', -y)
            if (! by_thumb)
                this._updateThumb()

            this.element.scroll()
        }
    },


    _updateThumb: function () {
        var thumb_state = this.options.calcThumb(
            this._content.outerHeight(),
            this.element.height(),
            this.offset()
        )

        var bar = this.element.children('.mui_scrollable_bar')

        if (! thumb_state.visible)
            bar.addClass('hidden')
        else
        {
            bar.removeClass('hidden')
            this._thumb.css({
                height: thumb_state.size,
                top: thumb_state.pos
            })
        }
    },

    handleResize: function() {
        if (! this._native)
            this._setScroll(this.offset())
    },

    offset: function() {
        if (this._native)
            return this.element.scrollTop()
        else
            return - this._content.position().top
    },

    percent: function (value) {
        if (value == undefined)
            return (this.offset() + this.element.height()/2) / this._content.height()

        this.scrollTo(
            value * this._content.height() - this.element.height()/2
        )
    },

    scrollBy: function(offset) {
        this._setScroll(this.offset() + offset)
    },

    scrollTo: function(offset_or_element) {
        if (typeof(offset_or_element) == 'number')
            this._setScroll(offset_or_element)
        else
        {
            var offset = offset_or_element.offset().top - this._content.offset().top

            if (offset_or_element.outerHeight() < this.element.outerHeight())
            {
                // if target is shorter than viewport, center it
                offset -= (this.element.outerHeight() - offset_or_element.outerHeight())/2
            }

            this._setScroll(offset)
        }
    },

    scrollToBottom: function () {
        this._setScroll(this._content.outerHeight())
    },

    content: function () {
        return this._content
    },

    html: function (html) {
        if (arguments.length == 0)
            return this._content.html()

        this._content.html(html || '')
        this.handleResize()
    },

    pushState: function () {
        this._states = this._states || []
        this._states.push(this.offset())
    },
    popState: function () {
        var scroll = this._states.pop()
        if (scroll != null)
            this.scrollTo(scroll)
    }
});
