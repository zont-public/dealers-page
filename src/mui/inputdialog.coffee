import './inputdialog.sass'

import $ from 'jquery'

import {escape_html} from 'common/common'

import './dialog'


export default ({title, message='', initial='', placeholder='', type='text', ok_text='OK',
                 cancel_text='отмена', below_text, below_html, checker}) ->
    result = $.Deferred()

    below_html = below_html ? escape_html(below_text) ? ''

    on_ok = ->
        if checker?
            dialog.mui_dialog('buttonsdisabled', on)

            checker input.val()
                .always -> dialog.mui_dialog('buttonsdisabled', off)
                .then =>
                    result.resolve input.val()
                    $(@).mui_dialog 'close'
                .fail (error) ->
                    $.mui.mui_dialog.errorbox
                        title: 'Ошибка'
                        message: error
        else
            result.resolve input.val()
            $(@).mui_dialog 'close'


    dialog = $("
        <div class='mui_inputdialog'>
            <h1>#{escape_html title}</h1>
            <div>
                <p>#{escape_html message}</p>
                <p><input class='mui_inputdialog_input' type='#{type}'
                          placeholder='#{escape_html placeholder}' value='#{escape_html initial}'/></p>
                <p>#{below_html}</p>
            </div>
        </div>
    ")
    .mui_dialog
        close: ->
            dialog.mui_dialog('destroy').remove()
            result.reject() if result.state() is 'pending'

        buttons: [
            {
                text: cancel_text
                click: -> $(@).mui_dialog 'close'

            }
            {
                text: ok_text
                click: on_ok
            }
        ]

    input = dialog.find('input')
    input.on 'keydown', (ev) ->
        if ev.keyCode == 13
            on_ok.apply dialog
    input.get(0).focus()

    return result.promise()
