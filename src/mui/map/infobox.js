import './_infobox.sass';

import $ from 'jquery';

import Class from '../class';

import {Overlay, MapLayer} from '../atlas/atlas';


export const InfoBox = Class(Overlay, {

    // determined by _infobox.sass
    _internalOffset: { x: -15 - 3, y: -7 },

    init: function (options) {
        InfoBox.superclass.init.apply(this)

        this.layer(MapLayer.InteractiveLayer)

        this._div = $('<div class="mui_map_infobox">')
            .append(
                $('<div class="mui_map_infobox_close">')
                    .click(this.close.bind(this))
            )
            .append('<div class="mui_map_infobox_sink">')
            .append('<div class="mui_map_infobox_content">')

        this.set(options)
    },

    destroy: function () {
        this._div.remove()
        delete this._div
    },

    attach: function (container) {
        $(container).append(this._div)
    },

    detach: function () {
        this._div.detach()
    },

    update: function () {
        if (! this.loc()) return
        if (! this.map()) return

        var pix = this.loc2pix(this._loc)
        if (! pix) return

        this._div.css({
            left: pix.x + this._internalOffset.x + this._offset.x,
            bottom: this._div.parent().height() - pix.y - this._internalOffset.y - this._offset.y
        })
    },


    open: function () { this.opened(true) },
    close: function () { this.opened(false) }

})
.implement(Class.EventSource)
.implement(Class.Properties({
    loc: {
        set: function (value) {
            this._loc = value
            this.update()
        }
    },

    offset: {
        value: { x: 0, y: 0 },
        set: function (value) {
            this._offset = value
            this.update()
        }
    },

    content: {
        set: function (value) {
            this._content = value
            this._div.children('.mui_map_infobox_content').empty().append(value)
        }
    },

    opened: {
        get: function () { return this._div.hasClass('mui_map_infobox_visible') },
        set: function (value) {
            if (value != this.opened())
            {
                this._div.toggleClass('mui_map_infobox_visible', value)

                if (value)
                    this.notify('opened')
                else
                {
                    this.notify('closing')
                    setTimeout(this.notifier('closed'), 200)
                }
            }
        }
    }
}));

export default InfoBox
