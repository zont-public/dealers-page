import './_combobox_v2.sass';

import $ from 'jquery';
import 'jquery-ui/ui/widget';
import 'jquery-ui/ui/position';


import 'mui/scroll';


$.widget('mui.combobox_v2', {
    _init: function () {
        this.dropdown = $('<div>', { 'class': 'mui_combobox_v2_dropdown' })
            .scrollable()
            .appendTo('body')

        this.content = this.dropdown.scrollable('content')



        var self = this

        this.element.addClass('mui_combobox_v2')
            .children().each(function () {
                $(this).addClass('mui_combobox_v2_option')
                    .detach().appendTo(self.content)
            })

        this.element
            .on('click', (function () {
                if (this.dropdown.hasClass('mui_combobox_v2_open'))
                    this.close()
                else
                    this.open()
                return false
            }).bind(this))


        this.content.on('click', '.mui_combobox_v2_option', function () {
            $(this).addClass('mui_combobox_v2_selected').siblings().removeClass('mui_combobox_v2_selected')
            self._updateValue()
        })


        if (this.element.attr('value') !== undefined)
            this.content.children('[value="' + this.element.attr('value') + '"]')
                .addClass('mui_combobox_v2_selected')
        else
            this.content.children(':first')
                .addClass('mui_combobox_v2_selected')


        this._updateValue()
    },

    destroy: function () {
        var self = this
        this.content.children().each(function () {
            $(this)
                .removeClass('mui_combobox_v2_option')
                .detach()
                .appendTo(self.element)
        })
        this.element.off('click')
        this.dropdown
            .scrollable('destroy')
            .remove()
        delete this.content
        delete this.dropdown
    },

    _updateValue: function () {
        var selected = this.content.children('.mui_combobox_v2_selected')

        this.element
            .html(selected.html())
            .attr('value', selected.attr('value'))
    },


    open: function () {
        if (this.element.hasClass('mui_combobox_v2_disabled')) return

        this.dropdown
            .addClass('mui_combobox_v2_open')
            .position({
                my: 'left top',
                at: 'left bottom',
                of: this.element
            })
            .width(this.element.innerWidth())
            .height(
                Math.min(this.content.outerHeight(), 200)
            )
            .scrollable('handleResize')


        $(document).one('click', this.close.bind(this))
    },

    close: function () {
        if (this.dropdown)
            this.dropdown.removeClass('mui_combobox_v2_open')
    },


    value: function (val) {
        if (arguments.length == 0)
            return this.element.attr('value') || null

        this.content.children('[value="' + val + '"]')
            .addClass('mui_combobox_v2_selected')
            .siblings().removeClass('mui_combobox_v2_selected')
        this._updateValue()
    }
})
