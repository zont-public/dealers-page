import './_tumbler.sass';

import $ from 'jquery';
import 'jquery-ui/ui/widget';
import 'jquery-ui/ui/widgets/draggable';


$.widget('mui.tumbler', {
    options: {
        checked: false,

        _boxWidth: 50,
        _handleWidth: 11,

        text: {
            off: 'Выкл',
            on:  'Вкл'
        },

        labelHidden: false
    },

    _create: function () {
        this.element
            .addClass('mui_tumbler')
            .toggleClass('mui_tumbler__label_hidden', !!this.options.labelHidden)
            .append(this.label = $('<div>', { 'class': 'mui_tumbler_label' }))
            .append(
                this.box = $('<div>', { 'class': 'mui_tumbler_box' })
                    .append(
                        this.bg = $('<div>', { 'class': 'mui_tumbler_bg' })
                    )
                    .append(
                        this.handle = $('<div>', { 'class': 'mui_tumbler_handle '})
                    )
            )
            .click((function (event) {
                this.checked(! this.options.checked, event)
            }).bind(this))

        this.handle.draggable({
            axis: 'x',
            containment: this.box,

            drag: (function () {
                this._updateBg()
            }).bind(this),

            stop: (function (event, ui) {
                this.checked(ui.position.left + this.options._handleWidth/2 > this.options._boxWidth/2, event)
            }).bind(this)
        })

        this.checked(!! this.options.checked)

        // this._updateBg()
    },

    _updateBg: function (val) {
        if (arguments.length == 0)
            this.bg.width(this.handle.position().left)
        else
            this.bg.width(val ? this.options._boxWidth : 0)
    },

    // _updateState: function () {
    //     this.checked(this.handle.position().left > 0)
    // },

    checked: function (val, event) {
        if (arguments.length == 0)
            return this.options.checked

        val = !!val

        var prev = this.options.checked
        this.options.checked = val


        if (val)
            this.handle.css('left', this.options._boxWidth - this.options._handleWidth + 1)
        else
            this.handle.css('left', 0)
        this._updateBg(val)

        this.label.text(val ? this.options.text.on : this.options.text.off)

        if (val != prev)
            this._trigger('change', event, { from_ui: event != null, checked: val })
    },

    _setOptions: function (options) {
        this.element.toggleClass('mui_tumbler__label_hidden', !!this.options.labelHidden)
    },

    destroy: function () {
        this.element.removeClass('mui_tumbler').empty()

        delete this.handle
        delete this.box
        delete this.bg
        delete this.label
    }
});
