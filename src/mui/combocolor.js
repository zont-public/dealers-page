import './_combocolor.sass';

import $ from 'jquery';
import 'jquery-ui/ui/widget';


import 'mui/combobox_v2';

$.widget('mui.combocolor', $.mui.combobox_v2, {

    _init: function () {
        $.mui.combobox_v2.prototype._init.apply(this, arguments)

        this.dropdown.addClass('mui_combocolor_dropdown')

        this.content.children().each(function () {
            $(this).append(
                $('<span class="mui_combocolor_color"></span>')
                    .css('background', $(this).attr('value'))
            )
        })

        this._updateValue()
    },

    destroy: function () {
        this.dropdown.removeClass('mui_combocolor_dropdown')
        this.content.children().each(function () {
            $(this).children('.mui_combocolor_color').remove()
        })
        $.mui.combobox_v2.prototype.destroy.apply(this, arguments)
    },

    open: function () {
        $.mui.combobox_v2.prototype.open.apply(this, arguments)
    }
})
