import './calendar.sass';

import $ from 'jquery';
import 'jquery-ui/ui/widget';


import {parseRusDate, rusDateOnly} from 'common/common';
import 'mui/scroll';


$.widget('mui.calendar', {
    options: {
        cellSize: 35,
        year: new Date().getFullYear(),
        monthShortNames: [ 'янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек' ],

        highlight: { a: null, b: null },

        _dragging: false,

        handlers: { },

        _scrollInterval: null,

        _table: null,
        _table_container: null,

        _autoScrollSpeed: 15
    },

    _create: function() {
        var self = this


        this.element
            .addClass('mui_calendar')
            .html(
                '<div class="mui_calendar_year">'
              +     '<div class="mui_calendar_year-prev mui_calendar_year-button">'
              +         '<div class="mui_calendar_arrow-left"></div>'
              +     '</div>'
              +     '<div class="mui_calendar_year-next mui_calendar_year-button">'
              +         '<div class="mui_calendar_arrow-right"></div>'
              +     '</div>'
              +     '<div class="mui_calendar_year-text"></div>'
              + '</div>'

              + '<div class="mui_calendar_head">'
              +     '<div class="mui_calendar_day-names">'
              +         '<div>пн</div><div>вт</div><div>ср</div><div>чт</div><div>пт</div><div class="mui_calendar_red">сб</div><div class="mui_calendar_red">вс</div>'
              +     '</div>'
              + '</div>'

              + '<div class="mui_calendar_table-container">'
              +     '<div class="mui_calendar_table"></div>'
              + '</div>'

              + '<div class="mui_calendar_controls">'
              +     '<div class="mui_calendar_fields">'
              +         '<input bound="from" type="text"/><div class="mui_calendar_divider">—</div><input bound="to" type="text"/>'
              +     '</div>'
              +     '<div class="mui_calendar_buttons">'
              +         '<button role="ok" disabled="1">выбрать</button>'
              +         '<button role="cancel">отмена</button>'
              +     '</div>'
              + '</div>'
            )
            .find('button')
                .click(function () {
                    self._trigger($(this).attr('role'))
                })
                .end()


        this.options._table_container = this.element.find('.mui_calendar_table-container')
            .scrollable()
            .scroll(function() {
                self._updateMonth()
            })
        this.options._table = this.options._table_container.find('.mui_calendar_table')

        this.options.handlers = {
            first: $('<div>', { 'class': 'mui_calendar_handler mui_calendar_handler-first' }),
            last : $('<div>', { 'class': 'mui_calendar_handler mui_calendar_handler-last' })
        }

        this._fillTable()




        this.element.find('.mui_calendar_year')
            .on('mousewheel', function(event, ui) {
                self.options.year -= ui
                self._fillTable()
            })

        this.element.find('.mui_calendar_year-prev').click(function() {
            self.options.year--
            self._fillTable()
        })
        this.element.find('.mui_calendar_year-next').click(function() {
            self.options.year++
            self._fillTable()
        })


        this.element.find('[bound="from"]').on('keydown', function (event) {
            if (event.keyCode === 13)
                self.element.find('[bound="to"]').focus();
        });
        this.element.find('[bound="to"]').on('keydown', function (event) {
            if (event.keyCode === 13)
                if (self.element.find('button[role="ok"]').prop('disabled') === false)
                    self._trigger('ok');
        });


        this.options._table
            .on('mousedown touchstart', '.mui_calendar_selected', function(event, ui) {
                self.options._dragging = true

                var td = $(this)
                var d = self._dateFromTD(td)

                if (d.getTime() === self.options.highlight.a.getTime())
                {
                    var t = self.options.highlight.a
                    self.options.highlight.a = self.options.highlight.b
                    self.options.highlight.b = t
                }


                self._startDrag()

                return false
            })
            .on('mousedown click', '.mui_calendar_td:not(.mui_calendar_selected)', function(event, ui) {
                var d = self._dateFromTD($(this))
                if (d === null) return false

                self.options._dragging = true

                if (event.shiftKey)
                {
                    var a_dist = Math.abs(self.options.highlight.a - d)
                    var b_dist = Math.abs(self.options.highlight.b - d)

                    if (a_dist < b_dist)
                    {
                        self.options.highlight.a = d

                        // need to swap for the case if user will than drag with shift
                        var t = self.options.highlight.a
                        self.options.highlight.a = self.options.highlight.b
                        self.options.highlight.b = t
                    }
                    else
                        self.options.highlight.b = d

                    self._updateHighlight()
                }
                else
                {
                    self.options.highlight.a = self.options.highlight.b = d
                    self._updateHighlight()
                }

                self._startDrag()

                return false
            })


        this.element.find('input')
            .on('change keyup paste', function() {
                var r = self._checkInputs()

                if (r[0]) // dates are ok
                {
                    self.options.highlight.a = r[1]
                    self.options.highlight.b = r[2]
                    self._updateHighlight(true)
                }
            })
            .on('mousewheel', function(event, ui) {
                var orig = $(this).val()
                var date = parseRusDate(orig)

                if (date)
                {
                    $(this).val(rusDateOnly(new Date(date.getTime() - ui * 24*60*60*1000)))
                    if (self._checkInputs()[0])
                        $(this).change()
                    else
                    {
                        $(this).val(orig)
                        self._checkInputs() // to remove error highlights
                    }
                }
            })
    },


    _createMonth: function (firstDay) {
        var monthNo = firstDay.getMonth()

        var html = '<div class="mui_calendar_month">'

        html += '<div class="mui_calendar_tr">'

        for (var i = 0; i < (firstDay.getDay()+6)%7; i++)
            html += '<div class="mui_calendar_td"></div>'

        var d = new Date(firstDay)
        while (d.getMonth() == monthNo)
        {
            html += '<div class="mui_calendar_td"'
            html += ' month="' + d.getMonth() + '"'
            html += ' day="' + d.getDate() + '"'
            html += '>' + d.getDate() + '</div>'

            d = new Date(d.getFullYear(), d.getMonth(), d.getDate() + 1)

            if (d.getDay() == 1 && d.getMonth() == monthNo)
                html += '</div><div class="mui_calendar_tr">'
        }

        for (var i = (d.getDay()+6)%7; i < 7; i++)
            html += '<div class="mui_calendar_td"></div>'

        html += '</div>'

        html += '<div class="mui_calendar_month_name">' + this.options.monthShortNames[monthNo] + '</div>'

        html += '</div>'
        return html
    },


    _fillTable: function() {
        var html = ''

        for (var m = 0; m < 12; m++)
            // Using 12:00 as workaround of missing hour 2014-01-01 00:00—01:00
            // This is probably a bug of Chrome under Windows
            html += this._createMonth(new Date(this.options.year, m, 1, 12))

        this.options._table.html(html)

        var today = new Date()
        this.options._table.find('[month="' + today.getMonth() + '"][day="' + today.getDate() + '"]').addClass('mui_calendar_today')

        this.options._table_container.scrollable('handleResize')

        this.element.find('.mui_calendar_year-text').text(this.options.year)

        this.options.highlight = {
            first: { day: null, month: null },
            last : { day: null, month: null }
        }

        this._updateHighlight()
    },

    _updateMonth: function() {
        var scroll = this.options._table_container.scrollable('offset')

        var startRowNo = Math.ceil(scroll / this.options.cellSize)
        // excluding
        var endRowNo = Math.floor((scroll + this.options._table_container.height()) / this.options.cellSize)

        var dayCount = {}
        this.options._table.find('.mui_calendar_tr').slice(startRowNo, endRowNo).each(function (i, tr) {
            $(this).children().each(function(j, td) {
                var month = $(this).attr('month')
                if (dayCount.hasOwnProperty(month))
                    dayCount[month]++
                else
                    dayCount[month] = 1
            })
        })

        var largestMonth = null
        var largestDayCount = 0
        for (var month in dayCount)
            if (dayCount[month] > largestDayCount)
            {
                largestMonth = month
                largestDayCount = dayCount[month]
            }

        this.options._table.find('.mui_calendar_td').removeClass('mui_calendar_td_hl').filter('[month="' + largestMonth + '"]').addClass('mui_calendar_td_hl')
    },

    _updateHighlightFromDragging: function(dontUpdateFields) {
        var draggingValues = this.options.draggingValues
        if (draggingValues.first.month < draggingValues.last.month || (draggingValues.first.month == draggingValues.last.month && draggingValues.first.day <= draggingValues.last.day))
        {
            this.options.highlight.first = draggingValues.first
            this.options.highlight.last = draggingValues.last
        }
        else
        {
            this.options.highlight.first = draggingValues.last
            this.options.highlight.last = draggingValues.first
        }
        this._updateHighlight(dontUpdateFields)
    },

    _updateHighlight: function(dontUpdateFields) {
        var first, last
        var highlight = this.options.highlight
        if (highlight.a <= highlight.b)
        { first = highlight.a; last = highlight.b }
        else
        { first = highlight.b; last = highlight.a }

        if (! dontUpdateFields)
        {
            if (first)
                this.element.find('input[bound=from]').val(rusDateOnly(first))
            if (last)
                this.element.find('input[bound=to]').val(rusDateOnly(last))
        }


        this.options._table.find('.mui_calendar_td').removeClass('mui_calendar_selected')

        this.options.handlers.first.detach()
        this.options.handlers.last.detach()

        var self = this
        this.options._table.find('.mui_calendar_td').each(function() {
            var d = self._dateFromTD($(this))

            if (first <= d && d <= last)
            {
                $(this).addClass('mui_calendar_selected')


                if (d.getTime() === first.getTime())
                    self.options.handlers.first.appendTo(this)
                if (d.getTime() === last.getTime())
                    self.options.handlers.last.appendTo(this)
            }
        })


        this.element.find('button[role="ok"]').prop('disabled', ! (first && last))
    },

    _checkInputs: function() {
        var input1 = this.element.find('input[bound=from]')
        var input2 = this.element.find('input[bound=to]')
        var both = input1.add(input2)

        var date1 = parseRusDate(input1.val())
        var date2 = parseRusDate(input2.val())

        input1.toggleClass('error', date1 == null)
        input2.toggleClass('error', date2 == null)

        if (date1 && date2)
            if (date1 > date2)
                both.addClass('error')

        return [! both.hasClass('error'), date1, date2]
    },

    _dateFromTD: function(td) {
        if (td.attr('day'))
            return new Date(this.options.year, parseInt(td.attr('month'), 10), parseInt(td.attr('day'), 10))
        return null
    },

    _startDrag: function() {
        var self = this
        function handle_mousemove(event) {
            var x = event.pageX || event.originalEvent.changedTouches[0].pageX;
            var y = event.pageY || event.originalEvent.changedTouches[0].pageY;

            var td = self.options._table
                .find('.mui_calendar_tr').eq(Math.floor((y - self.options._table.offset().top) / self.options.cellSize))
                .children().eq(Math.floor((x - self.options._table.offset().left)/ self.options.cellSize))

            var d = self._dateFromTD(td)
            if (d)
            {
                self.options.highlight.b = d
                self._updateHighlight()
            }
        }

        function on_move(event, ui)
        {
            var y = event.pageY || event.originalEvent.changedTouches[0].pageY;

            if (y < self.options._table_container.offset().top)
            {
                if (! self.options._scrollInterval)
                    self.options._scrollInterval = setInterval(function() {
                        self.options._table_container.scrollable('scrollBy', -self.options._autoScrollSpeed)
                        handle_mousemove(event)
                    }, 30)
            }
            else if (y > self.options._table_container.offset().top + self.options._table_container.height())
            {
                if (! self.options._scrollInterval)
                    self.options._scrollInterval = setInterval(function() {
                        self.options._table_container.scrollable('scrollBy', +self.options._autoScrollSpeed)
                        handle_mousemove(event)
                    }, 30)
            }
            else if (self.options._scrollInterval)
            {
                clearInterval(self.options._scrollInterval)
                self.options._scrollInterval = null
            }

            handle_mousemove(event)

            return false
        }

        $(document)
            .on('mousemove touchmove', on_move)
            .one('mouseup touchend', function() {
                self.options._dragging = false
                $(document).off('mousemove touchmove', on_move)

                if (self.options._scrollInterval)
                {
                    clearInterval(self.options._scrollInterval)
                    self.options._scrollInterval = null
                }
            })
    },



    firstDate: function () {
        var a = this.options.highlight.a
        var b = this.options.highlight.b
        if (!a || !b) return null
        return a < b ? a : b
    },

    lastDate: function () {
        var a = this.options.highlight.a
        var b = this.options.highlight.b
        if (!a || !b) return null
        return a > b ? a : b
    },

    updateUI: function () {
        this.options._table_container.scrollable('handleResize')
    },


    centerOnToday: function () {
        var today = new Date()
        var todayCell = this.options._table_container
            .find('[month="' + today.getMonth() + '"][day="' + today.getDate() + '"]')
        var todayOffset = todayCell.offset().top - this.options._table_container.offset().top

        this.options._table_container.scrollable('scrollBy',
            todayOffset - this.options._table_container.height()/2 + this.options.cellSize/2
        )
    },

    pushScroll: function () {
        this.options._table_container.scrollable('pushState')
    },
    popScroll: function () {
        this.options._table_container.scrollable('popState')
    }
});
