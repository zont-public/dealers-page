import './_popup.sass';

import $ from 'jquery';
import 'jquery-ui/ui/widget';
import 'jquery-ui/ui/position';


var opposite = {
    'left': 'right',
    'right': 'left',
    'top': 'bottom',
    'bottom': 'top'
}


$.widget('mui.popup', {
    options: {
        direction: 'right',
        arrowPosition: '50%'
    },

    _create: function() {
        this.element
            .addClass('mui_popup')
            .css('display', 'none')
            .append('<div class="mui_popup_arrow">')
            .on('mousedown touchstart', function (e) {
                e.stopPropagation()
            })
            .on('transitionend webkitTransitionEnd', (function (ev) {
                if (ev.target == ev.currentTarget)
                {
                    if (!this.element.hasClass('mui_popup_visible'))
                        this.element.css('display', 'none')
                }
            }).bind(this))
    },

    _init: function() {
        this.direction(this.options.direction)
    },

    destroy: function() {
        this.element
            .removeClass('mui_popup')
            .children('.mui_popup_arrow')
                .remove()
    },

    show: function (options) {
        this.element.css('display', 'block')

        if (options.position)
            this.element.position(options.position)

        this.element.addClass('mui_popup_visible')

        if (options.close_on_touch_outside !== false)
        {
            $('body').one('mousedown touchstart', (function () {
                this.hide()
            }).bind(this))
        }
    },

    hide: function() {
        this.element
            .removeClass('mui_popup_visible')
    },

    visible: function () {
        return this.element.hasClass('mui_popup_visible')
    },


    direction: function (dir) {
        this.options.direction = dir

        var property = {
            'left':   'top',
            'right':  'top',
            'top':    'left',
            'bottom': 'left'
        }[this.options.direction]

        this.element.find('.mui_popup_arrow')
            .css({ left: '', right: '', top: '', bottom: '' })
            .css(property, this.options.arrowPosition)

        this.element
            .removeClass('mui_popup_left mui_popup_right mui_popup_top mui_popup_bottom')
            .addClass('mui_popup_' + this.options.direction)
    }
})
