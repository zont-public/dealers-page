import './_range.sass';

import $ from 'jquery';
import 'jquery-ui/ui/widget';
import 'jquery-ui/ui/widgets/mouse';


$.widget('mui.range', $.ui.mouse, {
    options: {
        min: 0,
        max: 100,
        value: 0,

        integer: false,

        highlightRange: null,

        step: 10
    },


    _create: function () {
        this.element
            .addClass('mui_range')
            .append(
                $('<div>', { 'class': 'mui_range_bg' })
                    .append($('<div>', { 'class': 'mui_range_highlight' }))
            )
            .append(
                this.handle = $('<div>', { 'class': 'mui_range_handle' })
            )

            .on('mousedown', this._mouseDown.bind(this))

            .on('touchstart touchmove', (function (event) {
                var touches = event.originalEvent.changedTouches
                if (touches[0])
                    this._moveHandle(this._localCoordFromPage(touches[0].pageX, touches[0].pageY))

                event.preventDefault()
            }).bind(this))


        this._mouseMoveBinding = this._mouseMove.bind(this)

        this._updateHighlightRange()
    },


    _init: function () {
        this.value(this.options.value, false)
    },


    destroy: function () {
        this.element
            .removeClass('mui_range')
            .empty()

        delete this.handle
        delete this._mouseMoveBinding
    },


    _updateHighlightRange: function () {
        var hl = this.element.find('.mui_range_highlight')

        hl.toggle(!! this.options.highlightRange)

        if (this.options.highlightRange)
        {
            hl.css({
                left: ((this.options.highlightRange[0] - this.options.min) / (this.options.max - this.options.min) * 100) + '%',
                right: (100 - (this.options.highlightRange[1] - this.options.min) / (this.options.max - this.options.min) * 100) + '%'
            })
        }
    },


    _localCoordFromPage: function (pageX, pageY) {
        var bounds = this.element.get(0).getBoundingClientRect()
        return {
            x: (pageX - bounds.left) * this.element.outerWidth()  / (bounds.right - bounds.left),
            y: (pageY - bounds.top ) * this.element.outerHeight() / (bounds.bottom - bounds.top)
        }
    },


    _mouseDown: function (e) {
        e.preventDefault()

        this._moveHandle(this._localCoordFromPage(e.pageX, e.pageY))

        $(document)
            .one('mouseup', (function () {
                $(document).off('mousemove', this._mouseMoveBinding)
            }).bind(this))
            .on('mousemove', this._mouseMoveBinding)

        return true
    },

    _mouseMove: function (e) {
        this._moveHandle(this._localCoordFromPage(e.pageX, e.pageY))
        return true
    },

    _moveHandle: function (point) {
        var prev_value = this.options.value

        var pos = Math.min(Math.max(/*event.pageX - offset.left*/point.x, 0), this.element.width())
        var val = this.options.min + pos / this.element.width() * (this.options.max - this.options.min)

        if (this.options.step)
        {
            val = Math.round(val / this.options.step) * this.options.step
            pos = ((val - this.options.min) / (this.options.max - this.options.min) * 100) + '%'
        }


        this.options.value = this.options.integer ? parseInt(val, 10) : val


        this.handle.css({
            left: pos
        })
        if (this.options.value != prev_value)
            this._trigger('change')
    },

    value: function (value, fire_event) {
        var width = this.element.width()

        if (arguments.length == 0)
        {
            // var val
            // var cssleft = this.handle.css('left')

            // if (cssleft[cssleft.length-1] == '%')
            //     val = this.options.min + cssleft.substr(0, cssleft.length-1) / 100 * (this.options.max - this.options.min)
            // else
            //     val = this.options.min + this.handle.position().left / this.element.width() * (this.options.max - this.options.min)

            // if (this.options.step)
            //     val = Math.round(val / this.options.step) * this.options.step

            // return this.options.integer ? parseInt(val, 10) : val

            return this.options.value
        }
        else
        {
            value = Math.min(Math.max(value, this.options.min), this.options.max)

            this.options.value = value

            var t = (value - this.options.min) / (this.options.max - this.options.min)

            this.handle.css({
                left: ((value - this.options.min) / (this.options.max - this.options.min) * 100) + '%'
            })

            if (fire_event != false)
                this._trigger('change')
        }
    }
});
