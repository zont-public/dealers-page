import './_pageddialog.sass';

import $ from 'jquery';
import 'jquery-ui/ui/widget';

import 'mui/basedialog';


$.widget('mui.pageddialog', $.mui.basedialog, {
    options: {
        title: "",
        subtitle: "",

        rightpanelVisible: false,

        no_padding: false,

        pagelist_append: null
    },

    _create: function() {
        $.mui.basedialog.prototype._create.call(this)

        var self = this, options = this.options

        this.element.closest('.mui_basedialog')
            .addClass('mui_pageddialog')
            .toggleClass('mui_pageddialog_rightpanel_visible', !!this.options.rightpanelVisible)

        this.wrapper.prepend(
            $('<div class="mui_pageddialog_pages">')
                .append(
                    $('<div class="mui_pageddialog_title">')
                )
                .append(
                    $('<div class="mui_pageddialog_subtitle">')
                )
                .append(
                    $('<ul class="mui_pageddialog_tabs">').append(
                        this.element.children().map(function() {
                            var page = $(this)
                            return $('<li>')
                                .text(page.attr('pagetitle'))
                                .click(function() {
                                    self.selectPage(page)
                                })
                                .get(0)
                        })
                    )
                )
                .append('<div class="mui_pageddialog_pages_appendix">z</div>')
        )

        this.wrapper.prepend(
            $('<div class="mui_pageddialog_rightpanel_content">')
        )


        // this.element.wrap($('<div>', { 'class': 'mui_pageddialog_content' }))


        // this.element.after('<div class="mui_pageddialog_buttonbox">')
        this.element.children().addClass('mui_pageddialog_page')

        this.wrapper.find('.mui_pageddialog_pages').find('li')
            .first().click()
    },


    _init: function () {
        this.element.closest('.mui_basedialog').toggleClass('mui_pageddialog_nopadding', !!this.options.no_padding)

        $.mui.basedialog.prototype._init.call(this)

        this.rightpanelVisible(this.options.rightpanelVisible)

        this.element.closest('.mui_pageddialog').find('.mui_pageddialog_title').text(this.options.title)
        this.element.closest('.mui_pageddialog').find('.mui_pageddialog_subtitle').text(this.options.subtitle)

        this.element.closest('.mui_pageddialog').find('.mui_pageddialog_pages_appendix')
            .empty()
            .append(this.options.pagelist_append);


        this.content.find('.mui_pageddialog_buttonbox').remove()

        var self = this

        if (this.options.buttons.length > 0)
        {
            this.content.append(
                $('<div>', { 'class': 'mui_pageddialog_buttonbox' })
                    .buttonbox({
                        buttons: $.map(this.options.buttons, function(btn) {
                            return {
                                id: btn.id,
                                text: btn.text,
                                click: btn.click.bind(self.element)
                            }
                        })
                    })
            )
        }


        var tabs = this.wrapper.find('.mui_pageddialog_tabs').children()
        this.element.children().each((function (i, page) {
            tabs.eq(i).toggleClass('mui_pageddialog_tab_hidden', $(page).hasClass('mui_pageddialog_page_hidden'))
        }).bind(this))

        if (tabs.filter('.mui_pageddialog_selected').hasClass('mui_pageddialog_tab_hidden'))
            this.selectPage(this.element.children().not('.mui_pageddialog_page_hidden').eq(0))
    },


    destroy: function() {
        this.element.removeClass('mui_pageddialog_content').children().removeClass('mui_pageddialog_page')
        this.element.siblings().remove()

        $.mui.basedialog.prototype.destroy.call(this)
    },


    selectPage: function (page) {
        this.wrapper.find('.mui_pageddialog_pages ul').children().eq(page.index())
            .addClass('mui_pageddialog_selected').siblings().removeClass('mui_pageddialog_selected')
        page.show().siblings().hide()

        this._trigger('pageselected')
    },


    selectedTabIndex: function () {
        return this.element.closest('.mui_pageddialog').find('.mui_pageddialog_selected').index()
    },

    selectedTab: function () {
        var index = this.selectedTabIndex()
        if (index != null)
            return this.element.children().eq(index)
    },

    disabled: function (val) {
        this.element.closest('.mui_pageddialog').find('.mui_pageddialog_buttonbox').buttonbox('disabled', !!val)
    },

    rightpanelVisible: function (val) {
        if (arguments.length == 0)
            return this.element.closest('.mui_pageddialog').hasClass('mui_pageddialog_rightpanel_visible')

        this.element.closest('.mui_pageddialog').toggleClass('mui_pageddialog_rightpanel_visible', val)

        setTimeout(this.reposition.bind(this), 300)
    },

    rightpanel: function (val) {
        return this.element.closest('.mui_pageddialog').find('.mui_pageddialog_rightpanel_content')
    },

    button: function (id) {
        return this.element.closest('.mui_pageddialog').find('.mui_pageddialog_buttonbox').buttonbox('button', id)
    }
});
