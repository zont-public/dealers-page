import Class from '../class';

import {Overlay} from './atlas';


var Marker = Class(Overlay, {

    init: function (options) {
        Marker.superclass.init.apply(this)

        this.set(options);
    },

    element: function () { return this._div },

    destroy: function () {
        delete this._div;
    },

    attach: function(container) {
        container.appendChild(this._div);

        // This is to prevent flash of unpositioned marker
        this._div.style.display = 'none'
        this._positioned = false
    },

    detach: function() {
        if (this._div)
            this._div.parentNode.removeChild(this._div);
    },

    update: function() {
        if (! this.loc()) return;
        if (! this.map()) return;
        if (! this._div)  return;
        if (this.loc().x() == null || this.loc().y() == null) return

        var pix = this.loc2pix(this.loc());
        if (pix) {
            this._div.style.left = pix.x + 'px';
            this._div.style.top  = pix.y + 'px';

            if (! this._positioned)
            {
                this._positioned = true
                this.visible(this._visible)
            }
        }
    }
})
.implement(Class.EventSource)
.implement(Class.Properties({
    icon: {
        value: null,
        set: function (value) {
            var parent = null;
            if (this._div && this._div.parentNode)
            {
                parent = this._div.parentNode;
                this.detach();
            }

            if (typeof value == 'function')
                this._icon = value();
            else
                this._icon = value;

            this._div = this._icon.element();
            this._div.style.position = 'absolute';
            this._div.style.cursor = 'pointer';

            if (this._div.addEventListener)
                this._div.addEventListener('click', this.notifier('click'), false);
            else
                this._div.attachEvent('click', this.notifier('click'));

            if (parent)
                this.attach(parent);

            if (this._loc)
                this.update()
        }
    },

    loc: {
        set: function (value) {
            this._loc = value;
            this.update();
        }
    },

    visible: {
        value: true,
        set: function (value) {
            this._visible = value
            this._div.style.display = (this._positioned && value) ? 'block' : 'none'
        }
    }
}));


export default Marker;
