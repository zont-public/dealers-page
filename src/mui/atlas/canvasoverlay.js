import {Overlay, MapLayer} from './atlas';
import Class from '../class';


export default Class(Overlay, {

    init: function () {
        this.layer(MapLayer.GraphicsLayer)

        this.canvas = document.createElement('canvas')
        if (typeof G_vmlCanvasManager != 'undefined')
            G_vmlCanvasManager.initElement(this.canvas)

        this.canvas.style.position = 'absolute'

        this._canvasOffset = { x: 0, y: 0 }
    },

    destroy: function () {
        this.detach()
    },

    attach: function (container) {
        this.container = container
        container.appendChild(this.canvas)
    },

    detach: function () {
        if (this.canvas.parentNode)
            this.canvas.parentNode.removeChild(this.canvas)

        delete this.container
    },

    update: function () {
        this.redraw()
    },

    redraw: function() {
        if (! this._map) return
        if (! this._initialized) return
        if (! this._visible) return

        // this.canvas.width = this.canvas.width
        var ctx = this.canvas.getContext('2d')
        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
        ctx.fillStyle = '#2ababe'
        //ctx.fillRect(0, 0, this.canvas.width, this.canvas.height)

        var localOffset = this.local2container({x:0, y:0})
        if (! localOffset)
            return

        localOffset.x -= this._canvasOffset.x
        localOffset.y -= this._canvasOffset.y

        ctx.save()
        ctx.translate(localOffset.x, localOffset.y)
        this.draw(ctx, localOffset)
        ctx.restore()
    },

    draw: function(ctx, localOffset) {
    },

    viewportChanged: function () {
        this._initialized = true

        var localOffset = this.local2container({x:0, y:0})
        if (! localOffset)
        {
            // console.warn('localOffset == null')
            return
        }

        var reserve = 0.5

        var cont_w = this._map.container().offsetWidth
        var cont_h = this._map.container().offsetHeight

        var w = this.canvas.width  = cont_w * (1 + 2*reserve)
        var h = this.canvas.height = cont_h * (1 + 2*reserve)

        this._canvasOffset = {
            x: - cont_w * reserve,
            y: - cont_h * reserve
        }

        this.canvas.style.left = (this._canvasOffset.x - localOffset.x) + 'px'
        this.canvas.style.top  = (this._canvasOffset.y - localOffset.y) + 'px'

        this.canvas.style.width  = w + 'px'
        this.canvas.style.height = h + 'px'

        this.update()
    }

})
.implement(Class.Properties({

    visible: {
        value: true,
        set: function (value) {
            if (this._visible != value)
            {
                this._visible = value
                this.canvas.style.display = value ? 'block' : 'none'

                if (this._visible)
                    this.redraw()
            }
        }
    }
}));
