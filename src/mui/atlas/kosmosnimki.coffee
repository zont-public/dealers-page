import Class from 'common/class'

import {providers} from 'atlas/atlas'
import GoogleProvider from 'atlas/google'


export default KosmosnimkiProvider = Class GoogleProvider, {

    deferred_init: (container, state, onload) ->
        KosmosnimkiProvider.superclass.deferred_init.apply @, arguments

        maptype = new google.maps.ImageMapType
            getTileUrl: (point, zoom) ->
                server = 'abcdef'[Math.floor Math.random() * 6]
                "http://#{server}.tile.osm.kosmosnimki.ru/kosmo/#{zoom}/#{point.x}/#{point.y}.png"

            isPng: yes
            maxZoom: 18
            minzoom: 2
            name: 'Kosmosnimki'
            opacity: 1.0
            tileSize: new google.maps.Size 256, 256

        @gmap.mapTypes.set 'kosmosnimki', maptype
        @gmap.setMapTypeId 'kosmosnimki'

    supportedTypes: -> ['street']

}
KosmosnimkiProvider = KosmosnimkiProvider.implement Class.Properties {
    type:
        get: -> 'street'
        set: ->
}

providers.registerProvider 'kosmosnimki', KosmosnimkiProvider
