import Class from '../class';
import Icon from './icon';

export default Class(Icon, {
})
.implement(Class.Properties({
    url: {
        set: function (value) {
            this._url = value
            this._div.style.backgroundImage = 'url(' + this._url + ')'
        }
    }
}));
