import Class from 'common/class'

import {providers} from 'atlas/atlas'
import GoogleProvider from 'atlas/google'


export default DigimapProvider = Class GoogleProvider, {

    deferred_init: (container, state, onload) ->
        DigimapProvider.superclass.deferred_init.apply @, arguments

        maptype = new google.maps.ImageMapType
            getTileUrl: (point, zoom) ->
                "//tile.digimap.ru/rumap/#{zoom}/#{point.x}/#{point.y}.png"

            isPng: yes
            maxZoom: 18
            minzoom: 2
            name: 'Digimap'
            opacity: 1.0
            tileSize: new google.maps.Size 256, 256

        @gmap.mapTypes.set 'digimap', maptype
        @gmap.setMapTypeId 'digimap'

    supportedTypes: -> ['street']

}
DigimapProvider = DigimapProvider.implement Class.Properties {
    type:
        get: -> 'street'
        set: ->
}

providers.registerProvider 'digimap', DigimapProvider
