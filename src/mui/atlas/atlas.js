import Class from '../class';

export const array = {
    remove: function (array, value) {
        for (var i = 0; i < array.length; i++)
            if (array[i] == value)
            {
                array.splice(i, 1)
                i--
            }
        return array
    }
}



export const Accessor = function (getset) {
    return function () {
        if (arguments.length == 0)
            return getset.get.call(this)

        getset.set.call(this, arguments[0])
        return this
    }
}



export const Loc = Class({
    init: function() {
        switch (arguments.length)
        {
            case 1: {
                this.__x = arguments[0].x
                this.__y = arguments[0].y
                break
            }

            case 2: {
                this.__x = arguments[0]
                this.__y = arguments[1]
            }
        }
    },

    x: function() { return this.__x },
    y: function() { return this.__y },

    toString: function() {
        return this.__y.toFixed(6) + ', ' + this.__x.toFixed(6)
    },
    string: function () {
        return this.__y.toFixed(6) + ', ' + this.__x.toFixed(6)
    },

    equals: function (other) {
        return this.__x.toFixed(6) == other.__x.toFixed(6) && this.__y.toFixed(6) == other.__y.toFixed(6)
    }
})

Loc.prototype.parse = function (str) {
    var parts = str.split(',')
    return new Loc(parseFloat(parts[1]), parseFloat(parts[0]))
}





export const MapLayer = {
    GraphicsLayer: 'GraphicsLayer',
    OverlayLayer : 'OverlayLayer',
    InteractiveLayer: 'InteractiveLayer'
}



export const Overlay = Class({
    init: function () {
    },

    added: function () {},
    removed: function () {},

    attach: function (container) { },
    detach: function () { },
    update: function () { },
    viewportChanged: function () { this.update() },

    loc2pix: function (loc) { return this._map.loc2pix(this, loc) },
    local2container: function (pix) { return this._map.local2container(this, pix) },
    container2local: function (pix) { return this._map.container2local(this, pix) }
})
.implement(Class.Properties({
    'map': {
        value: null,
        set: function (value) {
            if (this._map == value)
                return

            // if (this._map && this.removed) this.removed()

            this._map = value

            // if (this._map && this.added) this.added()
        }
    },

    layer: { }
}))




var map_options = {
    type: 'street',
    center: new Loc(43.9, 56.3),
    zoom: 5,
    traffic: false,
    scrollZoom: true
}

export const Map = Class({

    init: function(options) {
        this._adapter = null

        this._provider = options.provider
        this._container = options.container
        this._initialized = false

        this._overlays = []

        this._initial_options = options

        this.recreate()
    },


    destroy: function () {
        this._provider = null;
        this.recreate();
    },


    initialize: function () {
        if (! this._initialized)
        {
            this._initialized = true
            this.recreate()
        }
    },


    recreate: function() {
        var state = {}

        if (! this._adapter)
        {
            for (var name in map_options)
                if (this._initial_options && this._initial_options.hasOwnProperty(name))
                    state[name] = this._initial_options[name]
                else
                    state[name] = map_options[name]
        }
        else
        {
            for (var name in map_options)
                state[name] = this.prop(name)
        }

        if (this._adapter)
        {
            this._adapter.unbindAll()
            this._adapter.destroy()
            this._adapter = null

            this._subcontainer.innerHTML = ''
            this._container.removeChild(this._subcontainer)
            delete this._subcontainer
        }

        if (this._provider && this._initialized)
        {
            this._subcontainer = document.createElement('div')
            this._subcontainer.style.height = '100%'
            this._container.appendChild(this._subcontainer)

            this.loading(true)
            this._adapter = new (this._provider)(this._subcontainer, state, (function () {
                this.loading(false)
            }).bind(this))

            for (var i = 0; i < this._overlays.length; i++)
            {
                this._adapter.addOverlay(this._overlays[i])
                this._overlays[i].viewportChanged()
            }

            const loc2container_bound = this.loc2container.bind(this);
            this._adapter.bind('mousemove', (pos) => {
                this.notify('mousemove', [pos, loc2container_bound]);
            });

            this._adapter.chain([
                // 'mousemove',
                'mousedown',
                'boundschange',
                'zoom',
                'projection_ready'
            ], this)
        }
    },


    container: function () { return this._container },


    provider: function() {
        if (arguments.length == 1) {
            if (this._provider != arguments[0])
            {
                this._provider = arguments[0]
                this.recreate()
            }
            return this
        }

        return this._provider
    },


    prop: function() {
        if (this._adapter)
        {
            if (arguments.length == 1)
                return this._adapter[arguments[0]]()
            else
                this._adapter[arguments[0]](arguments[1])
        }
        return this
    },


    addOverlay: function (overlay) {
        if (overlay.map())
            overlay.removed()

        this._overlays.push(overlay)

        overlay.map(this)

        if (this._adapter)
            this._adapter.addOverlay(overlay)

        overlay.added()
        overlay.update()
    },

    removeOverlay: function (overlay) {
        overlay.removed()
        overlay.map(null)

        array.remove(this._overlays, overlay)

        if (this._adapter)
            this._adapter.removeOverlay(overlay)
    },

    removeAllOverlays: function () {
        for (var i = this._overlays.length - 1; i >= 0; i--)
        {
            this.removeOverlay(this._overlays[i])
        }
    },

    loc2pix: function (overlay, loc) {
        if (this._adapter)
            return this._adapter.loc2pix(overlay, loc)
        return null
        // throw new Error("Can't use projection while before provider was set")
    },

    local2container: function (overlay, pix) {
        if (this._adapter)
            return this._adapter.local2container(overlay, pix)
        return null
        // throw new Error("Can't use projection while before provider was set")
    },

    container2local: function (overlay, pix) {
        if (this._adapter)
            return this._adapter.container2local(overlay, pix)
        return null
        // throw new Error("Can't use projection while before provider was set")
    },

    loc2container: function (loc) {
        if (this._adapter)
            return this._adapter.loc2container(loc);
        return null;
    },

    supportedTypes: function () { return this._adapter.supportedTypes() },

    handleResize: function () {
        if (this._adapter)
            this._adapter.handleResize()

        for (var i = 0; i < this._overlays.length; i++)
            this._overlays[i].viewportChanged()

        return this
    },

    orientation: function (loc1, loc2) { if (this._adapter) return this._adapter.orientation(loc1, loc2); else return null },


    addOffset: function () { if (this._adapter) return this._adapter.addOffset.apply(this._adapter, arguments); else return this },

    bounds_to_center_and_zoom: function (bounds) {
        var width = this._container.offsetWidth;
        var height = this._container.offsetHeight;
        if (width && height)
            return utils.bounds_to_center_and_zoom(bounds, [width, height]);
        else
            return null;
    },
    set_center_and_zoom: function (center, zoom, options) {
        if (this._adapter)
            this._adapter.set_center_and_zoom(center, zoom, options);
    }
})
.implement(Class.EventSource)
.implement(Class.Properties({
    type: {
        get: function () { if (this._adapter) return this._adapter.type.apply(this._adapter, arguments); else return this._initial_options.type },
        set: function () { if (this._adapter) return this._adapter.type.apply(this._adapter, arguments); else this._initial_options.type = arguments[0] }
    },
    center: {
        get: function () { if (this._adapter) return this._adapter.center.apply(this._adapter, arguments); else return this._initial_options.center },
        set: function () { if (this._adapter) return this._adapter.center.apply(this._adapter, arguments); else this._initial_options.center = arguments[0] }
    },
    zoom: {
        get: function () { if (this._adapter) return this._adapter.zoom.apply(this._adapter, arguments); else return this._initial_options.zoom },
        set: function () { if (this._adapter) return this._adapter.zoom.apply(this._adapter, arguments); else this._initial_options.zoom = arguments[0] }
    },

    bounds: {
        get: function () { if (this._adapter) return this._adapter.bounds.apply(this._adapter, arguments); else return this._initial_options.bounds },
        set: function () { if (this._adapter) return this._adapter.bounds.apply(this._adapter, arguments); else this._initial_options.bounds = arguments[0] }
    },

    traffic: {
        get: function () { if (this._adapter) return this._adapter.traffic.apply(this._adapter, arguments); else return this._initial_options.traffic },
        set: function () { if (this._adapter) return this._adapter.traffic.apply(this._adapter, arguments); else this._initial_options.traffic = arguments[0] }
    },

    scrollZoom: {
        get: function () { if (this._adapter) return this._adapter.scrollZoom.apply(this._adapter, arguments); else return this._initial_options.scrollZoom },
        set: function () { if (this._adapter) return this._adapter.scrollZoom.apply(this._adapter, arguments); else this._initial_options.scrollZoom = arguments[0] }
    },

    loading: { event: true }
}))



export const MapProvider = Class({}).implement(Class.EventSource)


export const utils = {
    calcBounds: function (locArray) {
        if (locArray.length == 0)
            return null

        if (locArray.length == 1)
            return [ locArray[0], locArray[0] ]

        var xmin = locArray[0].x(), xmax = locArray[0].x()
        var ymin = locArray[0].y(), ymax = locArray[0].y()

        for (var i = 1; i < locArray.length; i++)
        {
            var loc = locArray[i]
            if (xmin == null || loc.x() < xmin) xmin = loc.x()
            if (xmax == null || loc.x() > xmax) xmax = loc.x()
            if (ymin == null || loc.y() < ymin) ymin = loc.y()
            if (ymax == null || loc.y() > ymax) ymax = loc.y()
        }

        return [ new Loc(xmin, ymin), new Loc(xmax, ymax) ]
    },

    expandBounds: function (bounds, fraction)
    {
        if (! bounds) return bounds

        var w = Math.abs(bounds[0].x() - bounds[1].x())
        var h = Math.abs(bounds[0].y() - bounds[1].y())
        return [
            new Loc(Math.min(bounds[0].x(), bounds[1].x()) - w*fraction, Math.min(bounds[0].y(), bounds[1].y()) - h*fraction),
            new Loc(Math.max(bounds[0].x(), bounds[1].x()) + w*fraction, Math.max(bounds[0].y(), bounds[1].y()) + h*fraction)
        ]
    },

    bounds_to_center_and_zoom: function (bounds, [container_width, container_height]) {
        // (xspan ÷ 360) × (256 × 2^zoom) ≤ div_width
        // (yspan ÷ 180) × (256 × 2^zoom) ≤ div_height

        var xspan = Math.abs(bounds[0].x() - bounds[1].x());
        var yspan = Math.abs(bounds[0].y() - bounds[1].y());
        return [
            new Loc((bounds[0].x() + bounds[1].x()) / 2, (bounds[0].y() + bounds[1].y()) / 2),
            Math.floor(Math.min(
                Math.log(container_width * 360 / xspan / 256) / Math.log(2),
                Math.log(container_height * 180 / yspan / 256) / Math.log(2)
            ))
        ]
    }
}


export const providers = {
    _map: {},

    registerProvider: function (id, provider) {
        providers._map[id] = provider
    },

    byId: function (id) {
        return providers._map[id]
    },

    providerName: function (provider) {
        for (var i in providers._map)
            if (providers._map[i] == provider)
                return i
        return ''
    }
}



export const geometry = {
    distance: function (a, b) {
        var R = 6371000 // m
        var dLat = (b.y()-a.y()) * Math.PI / 180
        var dLon = (b.x()-a.x()) * Math.PI / 180
        var lat1 = a.y() * Math.PI / 180
        var lat2 = b.y() * Math.PI / 180

        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2)
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))
        return R * c
    },


    extrudeBounds: function (bounds, fraction) {
        if (!bounds)
            return null

        var xpad = (bounds[1].x() - bounds[0].x()) * fraction
        var ypad = (bounds[1].y() - bounds[0].y()) * fraction

        return [
            new Loc(bounds[0].x() - xpad, bounds[0].y() - ypad),
            new Loc(bounds[1].x() + xpad, bounds[1].y() + ypad)
        ]
    }
}
