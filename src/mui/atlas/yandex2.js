import $ from 'jquery';

import Class from '../class';
import {setImmediate} from 'setimmediate';

import {Loc, MapLayer, MapProvider, array, providers} from './atlas';


function to_y(loc) {
    return [loc.y(), loc.x()]
}
function from_y(geo) {
    return new Loc(geo[1], geo[0])
}

var type_to_y = {
    'street':    'yandex#map',
    'satellite': 'yandex#satellite',
    'hybrid':    'yandex#hybrid'
}
var type_from_y = {
    'yandex#map': 'street',
    'yandex#satellite': 'satellite',
    'yandex#hybrid': 'hybrid'
}


var OverlayProxy = new Class({
    init: function(ymap, parent) {
        this.parent = parent
        this.ymap = ymap

        var layer
        switch (this.parent.layer())
        {
            case MapLayer.GraphicsLayer: layer = 'places'; break;
            case MapLayer.InteractiveLayer: layer = 'overlaps'; break;
            default: layer = 'overlaps'
        }

        this.pane = ymap.panes.get(layer)
        this.parent.attach(this.pane.getElement())

        this.update_handler = this.update.bind(this)

        this.pane.events.add('actionend', this.update_handler)
        setImmediate(this.update_handler)
    },

    destroy: function() {
        this.pane.events.remove('actionend', this.update_handler)
        this.parent.detach()

        delete this.pane
        delete this.ymap
        delete this.update_handler
        delete this.parent
    },

    update: function () {
        if (this.parent)
            this.parent.viewportChanged()
        // viewportChanged() will call update()
        // this.parent.update()
    }
})


var Yandex2Provider = Class(MapProvider, {
    init: function (container, state, onload) {
        this.ymap = null
        this._overlays = []
        this._projection = null
        this._initial_state = state

        Yandex2Provider.when_ymaps_ready((function () {
            this.deferred_init(container, state, onload)
        }).bind(this))
    },

    deferred_init: function (container, state, onload) {
        this.ymap = new ymaps.Map(container, {
            type: 'yandex#map',
            center: [0, 0],
            zoom: 1,

            behaviors: ['default', 'scrollZoom'],
            controls: []
        })


        this._projection = this.ymap.options.get('projection')

        for (var i = 0, len = this._overlays.length; i < len; i++)
        {
            var overlay = this._overlays[i]
            overlay.__y2map_overlay = new OverlayProxy(this.ymap, overlay)
        }

        this.set(this._initial_state)


        var mouseEventHandler = function (context, eventname) {
            return (function (e) {
                var p = e.get('position')
                var containerOffset = this.ymap.container.getOffset()
                this.notify(eventname, [{
                    pix: {
                        x: p[0] - containerOffset[0],
                        y: p[1] - containerOffset[1]
                    },
                    loc: from_y(e.get('coords'))
                }])
            }).bind(context)
        }


        this.ymap.events.add('mousedown', mouseEventHandler(this, 'mousedown'))
        this.ymap.events.add('mousemove', mouseEventHandler(this, 'mousemove'))

        this.ymap.events.add('boundschange', this.notifier('boundschange'))
        this.ymap.events.add('boundschange', this.notifier('zoom'))

        this.notify('projection_ready')

        if (onload) onload()
    },

    destroy: function () {
        for (var i = 0; i < this._overlays.length; i++)
            this._detachOverlay(this._overlays[i])

        if (this.ymap)
            try
            {
                this.ymap.destroy()
            }
            catch (e) { }
    },



    supportedTypes: function () { return ['street', 'satellite', 'hybrid'] },


    addOverlay: function (overlay) {
        this._overlays.push(overlay)

        if (this.ymap)
            overlay.__y2map_overlay = new OverlayProxy(this.ymap, overlay)
    },

    _detachOverlay: function (overlay) {
        if (overlay.__y2map_overlay)
            overlay.__y2map_overlay.destroy()
        delete overlay.__y2map_overlay
    },

    removeOverlay: function (overlay) {
        this._detachOverlay(overlay)
        array.remove(this._overlays, overlay)
    },

    loc2pix: function (overlay, loc) {
        if (! this.ymap || !overlay.__y2map_overlay)
            return null

        var globalpix = this._projection.toGlobalPixels(to_y(loc), this.ymap.getZoom())
        var client = overlay.__y2map_overlay.pane.toClientPixels(globalpix)
        if (isNaN(client[0]))
            return null

        return {
            x: client[0],
            y: client[1]
        }
    },

    local2container: function (overlay, pix) {
        if (! this.ymap || !overlay.__y2map_overlay)
            return null

        var containerOffset = this.ymap.container.getOffset()
        var res = this.ymap.converter.globalToPage(
            overlay.__y2map_overlay.pane.fromClientPixels([pix.x, pix.y])
        )
        return {
            x: res[0] - containerOffset[0],
            y: res[1] - containerOffset[1]
        }
    },

    container2local: function (overlay, pix) {
        if (! this.ymap || !overlay.__y2map_overlay)
            return null

        var containerOffset = this.ymap.container.getOffset()
        var res = overlay.__y2map_overlay.pane.toClientPixels(
            this.ymap.converter.pageToGlobal([pix.x + containerOffset[0], pix.y + containerOffset[1]])
        )
        return { x: res[0], y: res[1] }
    },


    loc2container: function (loc) {
        if (! this.ymap) return null;
        var p = this.ymap.options.get('projection');
        if (! p) return null;
        var offset = this.ymap.container.getOffset();
        var page = this.ymap.converter.globalToPage(p.toGlobalPixels(to_y(loc), this.zoom()));
        return {
            x: page[0] - offset[0],
            y: page[1] - offset[1]
        };
    },


    handleResize: function () {
        if (this.ymap)
            this.ymap.container.fitToViewport(false)
    },


    orientation: function (loc1, loc2) {
        if (!this._projection)
            return null

        var p1 = this._projection.toGlobalPixels(to_y(loc1), 0)
        var p2 = this._projection.toGlobalPixels(to_y(loc2), 0)

        return Math.atan2(
            p2[0] - p1[0],
            p1[1] - p2[1]
        ) / Math.PI * 180
    },


    addOffset: function (loc, offset, zoom) {
        if (! this.ymap)
            return null

        // Move loc by (x,y) pixels at zoom offset
        var proj = this.ymap.options.get('projection')
        var wpix = proj.toGlobalPixels(to_y(loc), zoom)
        wpix[0] += offset.x
        wpix[1] += offset.y
        return from_y(proj.fromGlobalPixels(wpix, zoom))
    },

    set_center_and_zoom: function (center, zoom, {smooth} = {}) {
        if (this.ymap)
            this.ymap.setCenter(to_y(center), zoom, {
                duration: smooth ? 200 : 0
            });
        else {
            this._initial_state.center = center;
            this._initial_state.zoom = zoom;
        }
    }
})
.implement(Class.Properties({
    type: {
        get: function() {
            if (this.ymap)
                return type_from_y[this.ymap.getType()]
            else
                return this._initial_state.type
        },

        set: function (value) {
            if (this.ymap)
                this.ymap.setType(type_to_y[value])
            else
                this._initial_state.type = value
        }
    },

    center: {
        get: function() {
            if (this.ymap)
                return from_y(this.ymap.getCenter())
            else
                return this._initial_state.center
        },

        set: function (loc, params) {
            if (this.ymap)
            {
                // if (params && params.smooth)
                // {
                //     this.ymap.panTo(to_y(loc)).then(params.callback)
                // }
                // else
                // {
                //     this.ymap.setCenter(to_y(loc))
                // }
                this.ymap.setCenter(to_y(loc), undefined, {duration: params&&params.smooth ? 200 : 0});
            }
            else
                this._initial_state.center = loc
        }
    },

    zoom: {
        get: function() {
            if (this.ymap)
                return this.ymap.getZoom()
            else
                return this._initial_state.zoom
        },

        set: function (value, options) {
            if (this.ymap)
                this.ymap.setZoom(value, {
                    duration: (options && options.smooth) ? 200 : 0
                })
            else
                this._initial_state.zoom = value
        }
    },

    bounds: {
        get: function () {
            if (this.ymap)
            {
                var ybounds = this.ymap.getBounds()
                return [
                    from_y(ybounds[0]),
                    from_y(ybounds[1])
                ]
            }
            else
                return this._initial_state.bounds
        },
        set: function (value, maxZoom, instant) {
            if (this.ymap)
            {
                var yabounds = [ to_y(value[0]), to_y(value[1]) ]

                this.ymap.setBounds(yabounds, {
                    checkZoomRange: maxZoom ? false : true,
                    duration: instant ? 0 : 150
                })

                if (maxZoom)
                    if (this.ymap.getZoom() > maxZoom)
                        this.ymap.setZoom(maxZoom)
            }
            else
                this._initial_state.bounds = value
        }
    },

    traffic: {
        get: function () {
            if (this.ymap)
                return !!this._trafficProvider
            else
                return this._initial_state.traffic
        },
        set: function (value) {
            if (this.ymap)
            {
                if (!!value == !!this._trafficProvider) return

                if (value)
                {
                    this._trafficProvider = new ymaps.traffic.provider.Actual({}, { infoLayerShown: true })
                    this._trafficProvider.setMap(this.ymap)
                }
                else
                {
                    this._trafficProvider.setMap(null)
                    delete this._trafficProvider
                }
            }
            else
                this._initial_state.traffic = value
        }
    },

    scrollZoom: {
        get: function () {
            if (this.ymap)
                return this.ymap.behaviors.isEnabled('scrollZoom')
            else
                return this._initial_state.scrollZoom
        },
        set: function (value) {
            if (this.ymap)
                this.ymap.behaviors[value ? 'enable' : 'disable']('scrollZoom')
            else
                this._initial_state.scrollZoom = value
        }
    }
}))


var script_loading = false
var ready = false

Yandex2Provider.when_ymaps_ready = function (callback) {
    if (! script_loading)
        script_loading = $.getScript('https://api-maps.yandex.ru/2.1.64/?load=package.full&mode=release&lang=ru-RU')

    script_loading.done(function () {
        // ymaps.ready calls callback in the next tick, but we sometimes
        // want it to be called immediately to avoid flash of "loading..." messages
        if (ready)
        {
            setImmediate(callback)
        }
        else
            ymaps.ready(function () {
                ready = true
                callback()
            })
    })
}

providers.registerProvider('yandex', Yandex2Provider);

export default Yandex2Provider;
