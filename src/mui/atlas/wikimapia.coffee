import Class from 'common/class'

import {providers} from 'atlas/atlas'
import GoogleProvider from 'atlas/google'


export default WikimapiaProvider = Class GoogleProvider, {

    deferred_init: (container, state, onload) ->
        WikimapiaProvider.superclass.deferred_init.apply @, arguments

        maptype = new google.maps.ImageMapType
            getTileUrl: (point, zoom) ->
                x = point.x % (1<<zoom)
                y = point.y % (1<<zoom)
                srvnum = x % 4 + (y % 4) * 4
                "http://i#{srvnum}.wikimapia.org/?x=#{x}&y=#{y}&zoom=#{zoom}&lng=1"

            isPng: yes
            maxZoom: 18
            minzoom: 2
            name: 'Wikimapia'
            opacity: 1.0
            tileSize: new google.maps.Size 256, 256

        @gmap.mapTypes.set 'wikimapia', maptype
        @gmap.setMapTypeId 'wikimapia'

    supportedTypes: -> ['street']

}
WikimapiaProvider.implement Class.Properties {
    type:
        get: -> 'street'
        set: ->
}

providers.registerProvider 'wikimapia', WikimapiaProvider
