import $ from 'jquery';

import Class from '../class';

import {Loc, MapLayer, MapProvider, array, providers} from './atlas';


function to_g(loc) {
    return new google.maps.LatLng(loc.y(), loc.x())
}
function from_g(latlng)
{
    return new Loc(latlng.lng(), latlng.lat())
}


function type_to_g(type)
{
    switch (type)
    {
        case 'street':    return google.maps.MapTypeId.ROADMAP
        case 'satellite': return google.maps.MapTypeId.SATELLITE
        case 'hybrid':    return google.maps.MapTypeId.HYBRID
    }
}
function type_from_g(gtype)
{
    switch (gtype)
    {
        case google.maps.MapTypeId.ROADMAP  : return 'street'
        case google.maps.MapTypeId.SATELLITE: return 'satellite'
        case google.maps.MapTypeId.HYBRID   : return 'hybrid'
    }
}


var OverlayProxy = null

function define_OverlayProxy() {
    OverlayProxy = Class(google.maps.OverlayView, {

        init: function (parent) {
            google.maps.OverlayView.call(this)
            this.parent = parent
        },

        onAdd: function() {
            var pane = null

            switch (this.parent.layer())
            {
                case MapLayer.GraphicsLayer:
                    pane = this.getPanes().overlayLayer;
                    break;
                case MapLayer.InteractiveLayer:
                    pane = this.getPanes().overlayMouseTarget;
                    break;
                default:
                    pane = this.getPanes().markerLayer
            }

            this.parent.attach(pane)

            this.idle_listener = google.maps.event.addListener(this.getMap(), 'idle', this.parent.viewportChanged.bind(this.parent))
        },

        onRemove: function() {
            this.parent.detach()
            if (this.idle_listener)
                google.maps.event.removeListener(this.idle_listener)
        },

        draw: function() {
            if (! this._initialized)
            {
                this.parent.viewportChanged()
                this._initialized = true
            }
            else
                this.parent.viewportChanged()
        },

        loc2pix: function(loc) {
            if (! this._projection)
                this._projection = this.getProjection()
            if (this._projection)
                return this._projection.fromLatLngToDivPixel(loc)
            return null
        },

        local2container: function (pix) {
            var p = this.getProjection()
            if (p)
                return p.fromLatLngToContainerPixel(p.fromDivPixelToLatLng(pix))
            return null
        },

        container2local: function (pix) {
            return this.getProjection().fromLatLngToDivPixel(
                this.getProjection().fromContainerPixelToLatLng(pix)
            )
        }

    })
}


const GoogleProvider = Class(MapProvider, {

    init: function(container, state, onload) {
        this.gmap = null
        this._overlays = []
        this._initial_state = state
        this._dragging = false

        GoogleProvider.when_gmaps_ready((function () {
            this.deferred_init(container, state, onload)
        }).bind(this))
    },

    deferred_init: function (container, state, onload) {
        if (onload) onload()

        define_OverlayProxy()

        this.gmap = new google.maps.Map(container, {
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            gestureHandling: 'greedy'
        })

        this.converter_overlay = new google.maps.OverlayView();
        this.converter_overlay.draw = () => {};
        this.converter_overlay.setMap(this.gmap);

        for (var i = 0, len = this._overlays.length; i < len; i++)
        {
            var overlay = this._overlays[i]
            overlay.__gmap_overlay = new OverlayProxy(overlay)
            overlay.__gmap_overlay.setMap(this.gmap)
        }

        this.set(this._initial_state)


        var mouseEventHandler = function (context, eventname) {
            return (function (ev) {
                if (! this._dragging)
                    this.notify(eventname, [{
                        pix: ev.pixel,
                        loc: from_g(ev.latLng)
                    }])
            }).bind(context)
        }


        google.maps.event.addListener(this.gmap, 'dragstart', (function () { this._dragging = true  }).bind(this))
        google.maps.event.addListener(this.gmap, 'dragend',   (function () { this._dragging = false }).bind(this))

        google.maps.event.addListener(this.gmap, 'mousedown', mouseEventHandler(this, 'mousedown'))
        google.maps.event.addListener(this.gmap, 'mousemove', mouseEventHandler(this, 'mousemove'))
        google.maps.event.addListener(this.gmap, 'idle', this.notifier('boundschange'));
        google.maps.event.addListener(this.gmap, 'projection_changed', this.notifier('projection_ready'))
    },

    destroy: function() {
        for (var i = 0; i < this._overlays.length; i++)
            this._detachOverlay(this._overlays[i])

        if (this.gmap)
            google.maps.event.clearInstanceListeners(this.gmap)
    },


    supportedTypes: function () { return ['street', 'satellite', 'hybrid'] },


    addOverlay: function (overlay) {
        this._overlays.push(overlay)

        if (this.gmap)
        {
            overlay.__gmap_overlay = new OverlayProxy(overlay)
            overlay.__gmap_overlay.setMap(this.gmap)
        }
    },

    _detachOverlay: function (overlay) {
        if (overlay.__gmap_overlay)
        {
            overlay.__gmap_overlay.setMap(null)
            delete overlay.__gmap_overlay
        }
    },

    removeOverlay: function (overlay) {
        this._detachOverlay(overlay)
        array.remove(this._overlays, overlay)
    },

    loc2pix: function (overlay, loc) {
        if (! overlay.__gmap_overlay)
            return null
        return overlay.__gmap_overlay.loc2pix(to_g(loc))
    },

    local2container: function (overlay, pix) {
        if (! overlay.__gmap_overlay)
            return null
        return overlay.__gmap_overlay.local2container(pix)
    },

    container2local: function (overlay, pix) {
        if (! overlay.__gmap_overlay)
            return null
        return overlay.__gmap_overlay.container2local(pix)
    },


    loc2container: function (loc) {
        if (! this.gmap) return null;
        const p = this.converter_overlay.getProjection();
        return p.fromLatLngToContainerPixel(to_g(loc));
    },


    handleResize: function () {
        if (this.gmap)
            google.maps.event.trigger(this.gmap, 'resize')
    },


    orientation: function (loc1, loc2) {
        if (! this.gmap)
            return null

        var p = this.gmap.getProjection()

        if (! p) return null

        var p1 = p.fromLatLngToPoint(to_g(loc1))
        var p2 = p.fromLatLngToPoint(to_g(loc2))

        return Math.atan2(
            p2.x - p1.x,
            p1.y - p2.y
        ) / Math.PI * 180
    },


    addOffset: function (loc, offset, zoom) {
        if (! this.gmap)
            return null

        // Move loc by (x,y) pixels at zoom offset
        var proj = this.gmap.getProjection()

        var wpix = proj.fromLatLngToPoint(to_g(loc))
        wpix.x += offset.x / Math.pow(2, zoom)
        wpix.y += offset.y / Math.pow(2, zoom)

        return from_g(proj.fromPointToLatLng(wpix))
    },

    set_center_and_zoom: function (center, zoom, {smooth} = {}) {
        if (this.gmap) {
            this.gmap.setCenter(to_g(center));
            this.gmap.setZoom(zoom);
        }
        else {
            this._initial_state.center = center;
            this._initial_state.zoom = zoom;
        }
    }
})
.implement(Class.Properties({
    type: {
        get: function() {
            if (this.gmap)
                return type_from_g(this.gmap.getMapTypeId())
            else
                return this._initial_state.type
        },
        set: function(value) {
            if (this.gmap)
                this.gmap.setMapTypeId(type_to_g(value))
            else
                this._initial_state.type = value
        }
    },

    center: {
        get: function() {
            if (this.gmap)
                return from_g(this.gmap.getCenter())
            else
                return this._initial_state.center
        },
        set: function(loc, params) {
            if (this.gmap)
            {
                if (params && params.smooth)
                {
                    if (to_g(loc).equals(this.gmap.getCenter()))
                    {
                        if (params.callback) params.callback()
                    }
                    else
                    {
                        if (params.callback)
                            google.maps.event.addListenerOnce(
                                this.gmap, 'idle', params.callback
                            )
                        this.gmap.panTo(to_g(loc))
                    }
                }
                else
                    this.gmap.setCenter(to_g(loc))
            }
            else
                this._initial_state.center = loc
        }
    },

    zoom: {
        get: function() {
            if (this.gmap)
                return this.gmap.getZoom()
            else
                return this._initial_state.zoom
        },
        set: function(value) {
            if (this.gmap)
                this.gmap.setZoom(value)
            else
                this._initial_state.zoom = value
        }
    },

    bounds: {
        get: function () {
            if (this.gmap)
            {
                var gbounds = this.gmap.getBounds()
                return [
                    from_g(gbounds.getSouthWest()),
                    from_g(gbounds.getNorthEast())
                ]
            }
            else
                return this._initial_state.bounds
        },

        set: function (value, maxZoom, instant) {
            if (this.gmap)
            {
                var gbounds = new google.maps.LatLngBounds(to_g(value[0]), to_g(value[1]))

                if (maxZoom)
                    google.maps.event.addListenerOnce(
                        this.gmap,
                        'zoom_changed',
                        (function () {
                            if (this.gmap.getZoom() > maxZoom)
                                this.gmap.setZoom(maxZoom)
                        }).bind(this)
                    )

                if (this.gmap.getDiv().offsetWidth)
                    this.gmap.fitBounds(gbounds)
            }
            else
                this._initial_state.bounds = value
        }
    },

    traffic: {
        get: function () {
            if (this.gmap)
                return !!this._trafficLayer
            else
                return this._initial_state.traffic
        },
        set: function (value) {
            if (this.gmap)
            {
                if (!!value == !!this._trafficLayer) return

                if (value)
                {
                    this._trafficLayer = new google.maps.TrafficLayer()
                    this._trafficLayer.setMap(this.gmap)
                }
                else
                {
                    this._trafficLayer.setMap(null)
                    delete this._trafficLayer
                }
            }
            else
                this._initial_state.traffic = value
        }
    },

    scrollZoom: {
        value: true,
        get: function () {
            if (this.gmap)
                return this._scrollZoom
            else
                return this._initial_state.scrollZoom
        },
        set: function (value) {
            if (this.gmap)
            {
                this._scrollZoom = !!value
                this.gmap.setOptions({ scrollwheel: !!value })
            }
            else
                this._initial_state.scrollZoom = value
        }
    }
}))


var _gmaps_script = null

var _gmaps_load_callbacks = []
var _gmaps_loaded = false
window.__gmaps_load_callback = function () {
    _gmaps_loaded = true
    for (var i = 0, len = _gmaps_load_callbacks.length; i < len; i++)
        _gmaps_load_callbacks[i]()
}

GoogleProvider.when_gmaps_ready = function (callback) {
    $(function () {
        if (! _gmaps_script)
        {
            _gmaps_script = document.createElement('script')
            // _gmaps_script.setAttribute('src', '//maps.google.com/maps/api/js?sensor=false&libraries=geometry')
            _gmaps_script.setAttribute('src', 'https://maps.googleapis.com/maps/api/js?callback=__gmaps_load_callback&key=AIzaSyBFG97wPResvu8BSgAN1Y0l5Id_Waw687k')
            document.body.appendChild(_gmaps_script)
        }

        if (! _gmaps_loaded)
            _gmaps_load_callbacks.push(callback)
        else
            callback()
    })
}


providers.registerProvider('google', GoogleProvider);
export default GoogleProvider;
