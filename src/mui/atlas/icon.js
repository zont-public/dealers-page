import Class from '../class';


export default Class({
    init: function (options) {
        this._div = document.createElement('div')
        this._div.style.position = 'relative'
        this._div.style.backgroundRepeat = 'no-repeat'
        this.set(options)
    },

    element: function () { return this._div }
})
.implement(Class.Properties({
    className: {
        get: function (value) {
            return this._div.className
        },
        set: function (value) {
            this._div.className = value
        }
    },

    size: {
        set: function (value) {
            this._size = value
            this._div.style.width  = value.w + 'px'
            this._div.style.height = value.h + 'px'
        }
    },

    anchor: {
        value: { x: 0, y: 0 },
        set: function (value) {
            this._anchor = value
            this._div.style.marginLeft = (-value.x) + 'px'
            this._div.style.marginTop  = (-value.y) + 'px'
        }
    },

    offset: {
        value: { x: 0, y: 0 },
        set: function (value) {
            this._offset = value
            this._div.style.backgroundPosition = (-value.x) + 'px ' + (-value.y) + 'px'
        }
    }
}));
