(function () {

atlas.NavitelProvider = Class(atlas.GoogleProvider, {
    deferred_init: function (container, state, onload) {
        atlas.NavitelProvider.superclass.deferred_init.apply(this, arguments)

        var navitelMapType = new google.maps.ImageMapType({
            getTileUrl: function (point, zoom) {
                var xs = point.x.toString()
                var ys = (Math.pow(2, zoom) - point.y - 1).toString()
                var zs = zoom.toString()

                while (xs.length < 8) xs = '0' + xs
                while (ys.length < 8) ys = '0' + ys
                while (zs.length < 2) zs = '0' + zs

                return 'http://maps.navitel.su/navitms.fcgi?t=' + xs + ',' + ys + ',' + zs
            },
            isPng: true,
            maxZoom: 17,
            minZoom: 3,
            name: 'Navitel',
            opacity: 1.0,
            tileSize: new google.maps.Size(256, 256)
        })

        this.gmap.mapTypes.set('navitel', navitelMapType)
        this.gmap.setMapTypeId('navitel')
    },

    supportedTypes: function () { return ['street'] }
})
.implement(Class.Properties({
    type: {
        get: function () { return 'street' },
        set: function () { }
    }
}))

atlas.providers.registerProvider('navitel', atlas.NavitelProvider)

})();
