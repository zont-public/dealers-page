(function() {

atlas.Polyline = Class(atlas.CanvasOverlay, {
    init: function (options) {
        atlas.Polyline.superclass.init.apply(this)
        this.set(options)
    },

    destroy: function () {
    },

    added: function () {
        this.map_handlers = this._map.bind({
            mousemove: this._onMouseMove.bind(this)
        })
    },

    removed: function () {
        this.map_handlers.unbind()
    },


    draw: function (ctx, localOffset) {
        var w = this.canvas.width
        var h = this.canvas.height

        function outside(p1, p2) {
            return (p1.x + localOffset.x < 0 && p2.x + localOffset.x < 0) ||
                   (p1.x + localOffset.x > w && p2.x + localOffset.x > w) ||
                   (p1.y + localOffset.y < 0 && p2.y + localOffset.y < 0) ||
                   (p1.y + localOffset.y > h && p2.y + localOffset.y > h)
        }


        {
            var same = true

            if (!this._pix || this._pix.length < 2 || this._pix.length != this._points.length)
                same = false
            else
            {
                var p0 = this.loc2pix(this._points[0])
                var p1 = this.loc2pix(this._points[1])
                if (p0.x != this._pix[0].x || p0.y != this._pix[0].y ||
                    p1.x != this._pix[1].x || p1.y != this._pix[1].y)
                {
                    same = false
                }
            }

            if (!same)
            {
                this._pix = new Array(this._points.length);
                for (var i = 0; i < this._points.length; i++)
                    this._pix[i] = this.loc2pix(this._points[i])
            }
        }



        if (this._pix.length >= 2) {

            /*
            ctx.beginPath()
            ctx.moveTo(this._pix[0].x, this._pix[0].y)

            var count = 1

            var prev = this._pix[0]
            for (var i = 1; i < this._pix. length; i++)
            {
                if (parseInt(this._pix[i].x, 10) == parseInt(prev.x, 10) &&
                    parseInt(this._pix[i].y, 10) == parseInt(prev.y, 10))
                {
                }
                else
                {
                    if (outside(prev, this._pix[i]))
                        ctx.moveTo(this._pix[i].x, this._pix[i].y)
                    else
                        ctx.lineTo(this._pix[i].x, this._pix[i].y)
                    count++
                }

                prev = this._pix[i]
            }


            ctx.lineCap = 'round'
            ctx.lineJoin = 'round'
            ctx.lineWidth = this._width
            ctx.strokeStyle = this._color
            ctx.stroke()

            */

            var pathActive = false
            var x = this._pix[0].x
            var y = this._pix[0].y
            var current_color = ''

            ctx.lineCap = 'round'
            ctx.lineJoin = 'round'
            ctx.lineWidth = this._width


            var changeColor = function (color) {
                if (pathActive)
                    ctx.stroke()

                ctx.beginPath()
                pathActive = true
                ctx.moveTo(x, y)
                ctx.strokeStyle = color
                current_color = color
            }

            var prev = this._pix[0]

            var color_i = 0
            for (var i = 0; i < this._pix.length; i++)
            {
                while (color_i < this._color.length  &&  this._color[color_i][0] < i)
                    color_i++

                if (color_i < this._color.length &&
                    this._color[color_i][0] == i &&
                    this._color[color_i][1] != current_color)
                {
                    changeColor(this._color[color_i][1])
                }

                if (parseInt(this._pix[i].x, 10) == parseInt(prev.x, 10) &&
                    parseInt(this._pix[i].y, 10) == parseInt(prev.y, 10))
                {
                }
                else
                {
                    if (outside(prev, this._pix[i]))
                        ctx.moveTo(this._pix[i].x, this._pix[i].y)
                    else
                        ctx.lineTo(this._pix[i].x, this._pix[i].y)

                    x = this._pix[i].x
                    y = this._pix[i].y
                }

                prev = this._pix[i]
            }

            if (pathActive)
                ctx.stroke()


            if (this._showOrientation)
            {

                var l = new Array(this._pix.length - 1)
                for (var i = 0; i < l.length; i++)
                {
                    var dx = this._pix[i+1].x - this._pix[i].x
                    var dy = this._pix[i+1].y - this._pix[i].y
                    l[i] = Math.sqrt(dx*dx + dy*dy)
                }

                ctx.save()

                ctx.beginPath()

                var pi = 0
                var c = 0
                while (true) {
                    c += this._orientationTicksInterval

                    while (pi < l.length && l[pi] < c) {
                        c -= l[pi]
                        pi++
                    }

                    if (pi == l.length)
                        break


                    var p1 = this._pix[pi]
                    var p2 = this._pix[pi+1]

                    var dx = p2.x - p1.x
                    var dy = p2.y - p1.y

                    var angle = Math.atan2(dy, dx)

                    var ax = p1.x + c / l[pi] * dx
                    var ay = p1.y + c / l[pi] * dy

                    if (
                        (ax + localOffset.x > 0 && ax + localOffset.x < this.canvas.width ) &&
                        (ay + localOffset.y > 0 && ay + localOffset.y < this.canvas.height)
                       )
                    {
                        ctx.save()
                        ctx.translate(ax, ay)
                        ctx.rotate(angle)
                        ctx.moveTo(-4, -2)
                        ctx.lineTo(0, 0)
                        ctx.lineTo(-4, 2)
                        ctx.restore()
                    }
                }

                ctx.lineWidth = 1
                ctx.strokeStyle = this._orientationTicksColor
                ctx.stroke()

                ctx.restore()
            }
        }
    },


    _onMouseMove: function (e) {
        if (! this._handleMouseMove) return
        if (! this._pix) return
        if (! this._visible) return

        var p = this.container2local(e.pix)

        var nearPoints = findTrackPointsNear(this._pix, p)

        if (nearPoints.length > 0)
        {
            if (this._mouseInside)
                this.notify('mousemove', [ e, nearPoints ])
            else {
                this._mouseInside = true
                this.notify('mouseover')
                this.notify('mousemove', [ e, nearPoints ])
            }
        }
        else
        {
            if (this._mouseInside)
            {
                this._mouseInside = false
                this.notify('mouseout')
            }
        }


        function findTrackPointsNear(points, p)
        {
            function distance(a, b)
            {
                return Math.sqrt((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y))
            }


            var result = []

            var prev_accepted = false

            var hot_radius = 5

            for (var i = 0; i < points.length - 1; i++)
            {
                var accepted = false

                var p1 = (points[i])
                var p2 = (points[i+1])

                var d1 = distance(p1, p)
                var d2 = distance(p2, p)

                if (d1 < hot_radius  ||  d2 < hot_radius)
                    accepted = true
                else
                {
                    var v1x = p2.x - p1.x
                    var v1y = p2.y - p1.y
                    var v2x = p.x - p1.x
                    var v2y = p.y - p1.y

                    var v1l2 = v1x*v1x + v1y*v1y

                    var scal_p = v1x*v2x + v1y*v2y
                    var proj = scal_p / v1l2

                    if (proj >= 0 && proj <= 1)
                    {
                        var vec_p = v1x*v2y - v1y*v2x
                        var h = Math.abs(vec_p) / Math.sqrt(v1l2)

                        if (h < hot_radius)
                            accepted = true
                    }
                }


                if (accepted)
                    if (! prev_accepted)
                        result.push(i)

                prev_accepted = accepted
            }

            return result
        }
    }
})
.implement(Class.EventSource)
.implement(Class.Properties({
    points: {
        set: function (points) {
            this._points = points
            if (this.container)
                this.redraw()
        }
    },

    handleMouseMove: {
        value: false
    },

    color: {
        value: '#c4443b',
        set: function (value) {
            this._color = value
            // if (value.length === undefined)
            if (Object.prototype.toString.call(value) !== '[object Array]')
                this._color = [[0, value]]
            this.redraw()
        }
    },

    width: {
        value: 5,
        set: function (value) {
            this._width = value
            this.redraw()
        }
    },

    showOrientation: {
        value: false,
        set: function (value) {
            this._showOrientation = value
            this.redraw()
        }
    },

    orientationTicksInterval: {
        value: 20,
        set: function (value) {
            this._orientationTicksInterval = value
            this.redraw()
        }
    },

    orientationTicksColor: {
        value: '#000080',
        set: function (value) {
            this._orientationTicksColor = value
            this.redraw()
        }
    }
}))

})();
