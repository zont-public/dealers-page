import Class from '../class';

import GoogleProvider from './google';
import {providers} from './atlas';

var OSMProvider = Class(GoogleProvider, {

    deferred_init: function (container, state, onload) {
        OSMProvider.superclass.deferred_init.apply(this, arguments)

        var osmMapType = new google.maps.ImageMapType({
            getTileUrl: function (point, zoom) {
                var server = 'abc'[Math.floor(Math.random()*3)]
                return 'https://' + server + '.tile.openstreetmap.org/' + zoom + '/' + point.x + '/' + point.y + '.png'
            },
            isPng: true,
            maxZoom: 18,
            minZoom: 2,
            name: 'OpenStreetMap',
            opacity: 1.0,
            tileSize: new google.maps.Size(256, 256)
        })

        this.gmap.mapTypes.set('osm', osmMapType)
        this.gmap.setMapTypeId('osm')
    },

    supportedTypes: function () { return ['street'] }

})
.implement(Class.Properties({
    type: {
        get: function () { return 'street' },
        set: function () { }
    }
}))

providers.registerProvider('osm', OSMProvider);

export default OSMProvider;
