import './_inputfile.sass';

import $ from 'jquery';

import {plural} from 'common/common';


$(function() {

    $.mui = $.mui || {}

    var display_value = function () {
        var val;
        if (this.files.length > 1)
            val = `выбрано ${this.files.length} ${plural(this.files.length, 'файл', 'файла', 'файлов')}`;
        else if ($(this).val())
        {
            const parts = $(this).val().split(/[\/\\]/);
            val = parts[parts.length-1];
        }

        $(this).siblings('input[type="text"]').val(val || $(this).attr('placeholder'));
    }

    $.mui.input_file = {
        wrap: function (i, input) {
            $(input)
                .wrap('<span class="mui_inputfile_container"></span>')
                .after('<input class="mui_inputfile_field" type="text"/><button>обзор</button>')
                .change(display_value)
            display_value.apply(input)
        }
    }

    $('input[type="file"]:not(.no_mui)').each($.mui.input_file.wrap)

    $('input[type="file"]:not(.no_mui):disabled').siblings('button').attr('disabled', 1)
});
