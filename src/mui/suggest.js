import './_suggest.sass';

import $ from 'jquery';
import 'jquery-ui/ui/widget';
import 'jquery-ui/ui/position';


import 'mui/scroll';


$.widget('mui.suggest', {

    options: {
        showAll: false
    },

    _create: function () {
        this.element
            .addClass('mui_suggest')

        this._variants = this.element.attr('suggest').split('|')
        if (this._variants.length == 1 && this._variants[0] == '')
            this._variants = []

        this._popup = $(
            '<div class="mui_suggest_popup">'
          +     '<ul class="mui_suggest_popup_ul"></ul>'
          + '</div>'
        ).scrollable().appendTo('body')
        this._popup_ul = this._popup.find('.mui_suggest_popup_ul')

        this.element
            .on('keyup focus', this._showPopup.bind(this))
            .on('blur', (function () {
                this._popup.hide()
            }).bind(this))


        this._popup_ul.on('mousedown touchstart', 'li', (function (e) {
            this.element.val($(e.currentTarget).text())
            this._popup.hide()
        }).bind(this))
    },

    _showPopup: function (event) {
        var val = this.element.val()


        var matched = []

        if (this.options.showAll)
            matched = this._variants
        else
        {
            for (var i = 0; i < this._variants.length; i++)
            {
                if (this._variants[i].indexOf(val) == 0)
                    matched.push(this._variants[i])
            }
        }


        if (matched.length == 0)
            this._popup.hide()
        else
        {
            var html = ''
            for (var i = 0; i < matched.length; i++)
                html += '<li>' + matched[i].replace('<', '&lt;') + '</li>'

            this._popup_ul.html(html)

            this._popup
                .outerWidth(this.element.outerWidth())
                .show()
                .position({
                    my: 'left top',
                    at: 'left bottom',
                    of: this.element
                })
                .height(Math.min(this._popup_ul.height(), 200))
                .scrollable('handleResize')
        }
    }

});
