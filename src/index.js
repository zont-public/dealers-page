import './mui/common/reset.sass';
import './mui/mui.sass';
import './styles/dealers.sass';

import $ from 'jquery';
import hjson from 'hjson';
import {escape_html} from './common';
import Class from './mui/class';
import {Map, Loc, MapLayer} from './mui/atlas/atlas';
import GoogleProvider from './mui/atlas/google';
import ImageIcon from './mui/atlas/imageicon';
import Marker from './mui/atlas/marker';
import InfoBox from './mui/map/infobox';
import './mui/checkbox';

import marker_url from './styles/marker.png';


const dealers_adjust_city_selector_height = () => {
  return $('.dealers-list-content').css({
    'max-height': $('.dealers-list').height() - $('.dealers-list-header').outerHeight()
  });
};


const filter_dealers = () => {
  const list = $('.dealers-list-content');
  const products = $('.dealers-search-filter .mui_checkbox_checked').map((i, x) => $(x).attr('product')).toArray();

  list.find('li').addClass('hidden');
  list.find(products.map(prod => `li[products~="${prod}"]`).join(', ')).removeClass('hidden');
  list.find('li').each(function () { $(this).data('marker').visible(!$(this).hasClass('hidden'));});

  const search_query = $('.dealers-search-field').val().toLowerCase().trim();

  return list.find('.dealers-city').addClass('hidden').each(function () {
    const name_match = $(this).find('h4').text().toLowerCase().indexOf(search_query) !== -1;
    const filter_match = $(this).find('li:not(.hidden)').size() > 0;
    $(this).toggleClass('hidden', !name_match || !filter_match);
  });
};

const cmp = (a, b) => {
    if (a < b) return -1;
    if (a > b) return +1;
    return 0;
};

const cmp_by_title = (a, b) => cmp(a.title, b.title);

const to_array = item_or_array => {
    if (item_or_array == null)
        return [];
    if (typeof item_or_array === 'object' && 'length' in item_or_array)
        return item_or_array;
    return [item_or_array];
};

$(function() {
  $.ajax({
    method: 'GET',
    url: 'https://zont-online.ru/cp/partners/hjson?rnd=' + Math.random().toString(),
    // url: './dealers.json',
    success: cities_text => {
      let cities;
      try {
        if (typeof cities_text == 'string')
          cities = hjson.parse(cities_text);
        else
          cities = cities_text;
      }
      catch (error) {
          console.error("Can't parse dealers.json");
          console.error(error);
          throw error;
      }

      cities.sort(cmp_by_title);
      cities.forEach(city => city.dealers.sort(cmp_by_title));
      dealers_map_init(cities);
    }
  });
  $('.dealers-search-field').on('keyup', filter_dealers);
  $('.dealers-search-filter .mui_checkbox').on('change', filter_dealers);
});

const dealers_map_init = cities => {
  const dealer_icon = Class.Factory(ImageIcon, [
    {
      url: marker_url,
      size: {
        w: 41,
        h: 40
      },
      anchor: {
        x: 12,
        y: 34
      }
    }
  ]);
  let active_marker = null;
  const infobox = new InfoBox({
    offset: {
      x: 0,
      y: -34
    }
  });
  infobox.bind({
    closing: () => {
      if (active_marker != null)
        active_marker.visible(true);
      active_marker = null;
    }
  });

  const map = new Map({
    container: $('.dealers-map-map').get(0),
    provider: GoogleProvider,
    type: 'street',
    scrollZoom: true
  });
  map.addOverlay(infobox);
  map.initialize();

  const list_div = $('.dealers-list-content');
  cities.forEach(city => {
    const item = $(
        '<div class="dealers-city"> <h4>' + escape_html(city.title) + '</h4> <div class="dealers-city-dealers"> <ul></ul> </div> </div>'
    ).data(city).appendTo(list_div);

    const ul = item.find('ul');

    city.dealers.forEach(dealer => {
      const a = $('<a href="#"></a>').text(dealer.title);
      const li = $('<li>').data(dealer).attr('products', dealer.products.join(' '));
      ul.append(li.append(a));

      const marker = new Marker({
        icon: dealer_icon,
        loc: new Loc(dealer.x, dealer.y),
        layer: MapLayer.InteractiveLayer
      });
      map.addOverlay(marker);

      ((marker, dealer) =>
        marker.bind({
          click: function() {
            return show_infobox(
                marker,
                dealer.title,
                dealer.content,
                dealer.phones,
                dealer.sites,
                dealer.emails,
                dealer.products.join(' ')
            );
          }
        })
      )(marker, dealer);

      li.data('marker', marker);
    });
  });

  dealers_adjust_city_selector_height();

  filter_dealers();


  const show_infobox = (marker, title, content, phones, sites, emails, products) => {
    if (active_marker != null)
      active_marker.visible(true);

    const list_to_html = list => '<p>' + list.join('</p><p>') + '</p>';

    infobox.set({
      loc: marker.loc(),
      content: $('<div>')
        .append($('<h3>').text(title))
        //.append(`<p>${to_array(phones).map(ph => }</p>`);
        .append(list_to_html(to_array(content)))
        .append(list_to_html(to_array(phones).map(ph => `Телефон: ${ph}`)))
        .append(list_to_html(to_array(sites).map(site => `Сайт: <a target="_blank" href='${site}'>${site.replace(/^https?:\/\//, '').replace(/\/$/, '')}</a>`)))
        .append(list_to_html(to_array(emails).map(mail => `E-Mail: <a href='mailto:${mail}'>${mail}</a>`)))
        .append(
            '<div class="products">' +
            ' <span class="product" product="ztc">Автосигнализации</span>' +
            ' <!-- <span class="product" product="zth">Отопление</span> -->' +
            ' <span class="product" product="zta">Охрана дома</span>' +
            ' <span class="product" product="h1000">Отопление</span>' +
            '</div>'
        )
        .find('a').attr('target', '_blank').end()
        .find('.products').attr('products', products).end()
    });

    infobox.open();

    active_marker = marker;
    map.center(marker.loc(), { smooth: true });
  };


  $(document).on('click', '.dealers-list-content h4', function() {
    const item = $(this).closest('.dealers-city');
    map
        .center(new Loc(item.data('x'), item.data('y')))
        .zoom(item.data('z'));

    infobox.close();

    item.find('.dealers-city-dealers').slideToggle(300);
    item.siblings().removeClass('dealers-city-selected');
    item.siblings().find('.dealers-city-dealers').slideUp(300);
    item.addClass('dealers-city-selected');

    setTimeout(dealers_adjust_city_selector_height, 310);

    return false;
  });

  return $(document).on('click', '.dealers-city-dealers li', function() {
    const $this = $(this);
    show_infobox(
        $this.data('marker'),
        $this.data('title'),
        $this.data('content'),
        $this.data('phones'),
        $this.data('sites'),
        $this.data('emails'),
        $this.attr('products')
    );
    return false;
  });
};
